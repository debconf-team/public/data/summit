#!/bin/sh
sh=''''
    dir=$(readlink -f $(dirname $0)/../env/bin)
    if [ -n "$dir" ]; then
        export PATH=$dir:$PATH
    fi
    script=$(readlink -f $0)
    cd $(dirname $0)/../summit
    exec python "$script" "$@"
' '''

import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

extra_paths = [
    os.path.abspath('..'),
    os.path.join(os.path.abspath('..'), 'summit')
]
for path in extra_paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'debconf_settings'

import datetime
import pytz
import csv

from summit.schedule.models import Summit, Slot, Meeting, Room, Agenda

summit = 'debconf14'
summit = Summit.objects.get(name=summit)

agenda = csv.reader(open('../scripts/dc14-agenda.csv', 'r'))

for item in agenda:
    title = item[0]
    date = item[1]
    start = item[2]
    room_id = item[3]

    print "room: %s date: %s time:%s meeting: %s" % (room_id, date, start, title)

    date_start = '2014-08-' + date + ' ' + start 
    datetime_start = datetime.datetime.strptime(date_start, '%Y-%m-%d %H:%M')
    slottime = pytz.timezone(summit.timezone).localize(datetime_start)
    start_utc = summit.delocalize(slottime)
    slot_id = Slot.objects.get(start_utc=start_utc).pk
    
    if not room_id: 
        room_id = Room.objects.get(name='room-327-8-9').pk
    elif room_id == '1':
        room_id = Room.objects.get(name='room-327').pk
    elif room_id == '2':
        room_id = Room.objects.get(name='room-328').pk
    elif room_id == '3':
        room_id = Room.objects.get(name='room-329').pk

    meeting_id = Meeting.objects.get(title__contains=title).pk
    

    print "room_id: %s slot_id: %s meeting_id: %s" % (room_id, slot_id, meeting_id)
    ai = Agenda.objects.create(room_id=room_id,slot_id=slot_id,meeting_id=meeting_id)
    print "created agenda item: %s\n" % ai
