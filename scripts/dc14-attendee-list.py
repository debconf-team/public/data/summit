#!/bin/sh
sh=''''
    dir=$(readlink -f $(dirname $0)/../env/bin)
    if [ -n "$dir" ]; then
        export PATH=$dir:$PATH
    fi
    script=$(readlink -f $0)
    cd $(dirname $0)/../summit
    exec python "$script" "$@"
' '''

import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

import datetime

extra_paths = [
    os.path.abspath('..'),
    os.path.join(os.path.abspath('..'), 'summit')
]
for path in extra_paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'debconf_settings'

from debconf_website.models import UserProfile

attendees = UserProfile.objects.filter(
                 attend=True, summit__name='debconf14'
            )

print '"Full Name","Nickname"'
for person in attendees:
    print '"%s","%s"' \
          % (person.badge_full,
             person.badge_nick
            )
