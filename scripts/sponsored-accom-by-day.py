#!/bin/sh
sh=''''
    dir=$(readlink -f $(dirname $0)/../env/bin)
    if [ -n "$dir" ]; then
        export PATH=$dir:$PATH
    fi
    script=$(readlink -f $0)
    cd $(dirname $0)/../summit
    exec python "$script" "$@"
' '''

import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

import datetime

extra_paths = [
    os.path.abspath('..'),
    os.path.join(os.path.abspath('..'), 'summit')
]
for path in extra_paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'debconf_settings'

from debconf_website.models import UserProfile

sponsorees = UserProfile.objects.filter(
                 attend=True, reconfirm=True, arrived=True 
             ).exclude(
                 sponsorship=None
             ).exclude(
                 sponsorship__needs_accomodation=False,
             )

leave_after = datetime.datetime(2014,8,20,18)
arrive_before = datetime.datetime(2014,8,21,13)
last_meal = datetime.datetime(2014,9,1,12)

print '"Dates (dinner->lunch)","Total"'
while leave_after < last_meal:
    present = sponsorees.filter(
                  start_utc__lte=arrive_before
              ).filter(
                  end_utc__gt=leave_after
              )
    print '"%s to %s","%s"' \
          % (leave_after.strftime('%Y-%m-%d'),
             arrive_before.strftime('%Y-%m-%d'),
             present.count()
            )

    leave_after += datetime.timedelta(1)
    arrive_before += datetime.timedelta(1)

