#!/bin/sh
sh=''''
    dir=$(readlink -f $(dirname $0)/../env/bin)
    if [ -n "$dir" ]; then
        export PATH=$dir:$PATH
    fi
    script=$(readlink -f $0)
    cd $(dirname $0)/../summit
    exec python "$script" "$@"
' '''

import sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

extra_paths = [
    os.path.abspath('..'),
    os.path.join(os.path.abspath('..'), 'summit')
]
for path in extra_paths:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'debconf_settings'

from debconf_website.models import UserProfile

sponsorees = UserProfile.objects.filter(
                 attend=True, reconfirm=True 
             ).exclude(
                 sponsorship=None
             ).exclude(
                 sponsorship__needs_food=False,
                 sponsorship__needs_accomodation=False
             ).order_by('badge_full')

print '"Name","Email","Roommate","Sex","Diet","Accommodation","Food","Arrival","Departure","Day Trip?"'
for i in sponsorees:
    print '"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"' \
          % (i.badge_full,
             i.contact_email,
             i.sponsorship.roommate,
             i.sex,
             i.diet,
             i.sponsorship.needs_accomodation,
             i.sponsorship.needs_food,
             i.start_utc,
             i.end_utc,
             i.daytrip,
            )
