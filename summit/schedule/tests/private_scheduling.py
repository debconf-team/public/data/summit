# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Agenda,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class PrivateSchedulingTestCase(djangotest.TestCase):

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(Summit, name='uds-test')
        self.slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+(2*one_hour),
            type='open',
            summit=self.summit
        )

        self.open_room = factory.make_one(
            Room,
            summit=self.summit,
            type='open'
        )
        self.public_meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting1',
            private=False,
            requires_dial_in=False,
            approved='APPROVED'
        )

        self.private_room = factory.make_one(
            Room,
            summit=self.summit,
            type='private'
        )
        self.private_meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting2',
            private=True,
            requires_dial_in=False,
            approved='APPROVED'
        )

        self.user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User'
        )
        self.attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user,
            start_utc=now,
            end_utc=now+week
        )

    def tearDown(self):
        pass

    def assertRaises(self, exception_type, function, args):
        try:
            function(*args)
            raise AssertionError(
                'Callable failed to raise exception %s' % exception_type
            )
        except exception_type, e:
            return True

    def run_autoschedule(self):
        from django.core.management import execute_from_command_line
        execute_from_command_line(
            argv=['manage.py', 'autoschedule', 'uds-test', '-v', '2']
        )

    def test_private_meeting_schedule(self):
        '''
        General run of the autoschedule, no gurantee of what ends up where
        '''
        self.run_autoschedule()

        # Private meetings should not ever be autoscheduled
        self.assertEquals(
            0,
            Agenda.objects.filter(
                slot__summit=self.summit,
                meeting__private=True,
                room__type='open'
            ).count()
        )
        self.assertEquals(
            0,
            Agenda.objects.filter(
                slot__summit=self.summit,
                meeting__private=False,
                room__type='private'
            ).count()
        )
        # Private rooms should not ever be autoscheduled
        self.assertEquals(
            0,
            Agenda.objects.filter(
                slot__summit=self.summit,
                meeting__private=True,
                room__type='private'
            ).count()
        )

        # Public meetings in open rooms should be autoscheduled
        self.assertEquals(
            1,
            Agenda.objects.filter(
                slot__summit=self.summit,
                meeting__private=False,
                room__type='open'
            ).count()
        )

    def test_no_available_public_room(self):
        '''
        Make sure public meetings will not be autoscheduled into private rooms
        '''
        self.open_room.type = 'private'
        self.open_room.save()

        self.run_autoschedule()

        # Without an open room, public meetings should not be autoscheduled
        self.assertEquals(0, self.public_meeting.agenda_set.count())

        # Private meetings should not ever be autoscheduled
        self.assertEquals(0, self.private_meeting.agenda_set.count())

        # Private rooms should not ever be autoscheduled
        self.assertEquals(
            0,
            Agenda.objects.filter(
                room__type='private'
            ).count()
        )

    def test_no_available_private_room(self):
        '''
        Make sure private meetings will not be autoscheduled into open rooms
        '''
        self.private_room.type = 'open'
        self.private_room.save()

        self.run_autoschedule()

        # Private meetings should not ever be autoscheduled
        self.assertEquals(0, self.private_meeting.agenda_set.count())

        # Public meeting should be autoscheduled into one of the two open rooms
        self.assertEquals(1, self.public_meeting.agenda_set.count())
        self.assertEquals(1, Agenda.objects.filter(room__type='open').count())

    def test_required_participant_in_private_meeting(self):
        '''
        Make sure meetings aren't scheduled when
        someone is in a private meeting.
        '''
        # Make the same person required for both meetings
        self.public_meeting.participant_set.create(
            attendee=self.attendee,
            participation='REQUIRED'
        )
        self.private_meeting.participant_set.create(
            attendee=self.attendee,
            participation='REQUIRED'
        )
        # Schedule the private meeting in the one available slot
        self.private_room.agenda_set.create(
            slot=self.slot,
            meeting=self.private_meeting
        )

        self.run_autoschedule()

        # Check that the private meeting is still scheduled
        self.assertEquals(
            1,
            self.private_meeting.agenda_set.count()
        )
        # Check that the public meeting is not scheduled
        self.assertEquals(0, self.public_meeting.agenda_set.count())
