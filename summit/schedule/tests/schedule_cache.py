# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.core.cache import cache

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Track,
    Room,
    Agenda,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class ScheduleCacheTestCase(djangotest.TestCase):

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(Summit, name='uds-test', timezone='UTC')
        self.slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+(2*one_hour),
            type='open',
            summit=self.summit
        )

        self.track = factory.make_one(
            Track,
            slug='test.track',
            summit=self.summit
        )
        self.room = factory.make_one(
            Room,
            name='test.room',
            title='Test Room',
            summit=self.summit
        )
        self.room.tracks = [self.track]
        self.meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            title='Test Meeting',
            name='meeting',
            private=False,
            requires_dial_in=False
        )
        self.meeting.tracks = [self.track]
        self.agenda = factory.make_one(
            Agenda,
            slot=self.slot,
            meeting=self.meeting,
            room=self.room
        )

        self.user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User'
        )
        self.attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user,
            start_utc=now,
            end_utc=now+week
        )

    def tearDown(self):
        # Cached requests cause render.py to return old data.
        # Let's clear the cache
        if hasattr(cache, 'clear'):
            cache.clear()
        # Older django didn't have .clear, but locmem cache did have ._cull
        elif hasattr(cache, '_cull'):
            cache._cull()

    def request_schedule_by_date(self):
        schedule_args = [self.summit.name, self.agenda.slot.start_utc.date()]
        schedule_url = reverse(
            'summit.schedule.views.by_date',
            args=schedule_args
        )
        response = self.client.get(schedule_url)
        return response

    def request_schedule_by_track(self):
        schedule_args = [self.summit.name, self.track.slug]
        schedule_url = reverse(
            'summit.schedule.views.by_track',
            args=schedule_args
        )
        response = self.client.get(schedule_url)
        return response

    def test_cache_cleared_on_meeting_change(self):
        self.assertEqual(None, cache.get('meeting-html-%s' % self.meeting.id))
        response = self.request_schedule_by_date()
        self.assertTrue(
            'Test Meeting' in cache.get(
                'meeting-html-%s' % self.meeting.id,
                ''
            )
        )

        self.meeting.save()

        self.assertEqual(None, cache.get('meeting-html-%s' % self.meeting.id))
        response = self.request_schedule_by_date()
        self.assertTrue(
            'Test Meeting' in cache.get(
                'meeting-html-%s' % self.meeting.id,
                ''
            )
        )

    def test_cache_cleared_on_agenda_change(self):
        self.assertEqual(None, cache.get('meeting-html-%s' % self.meeting.id))
        response = self.request_schedule_by_date()
        self.assertTrue(
            'Test Meeting' in cache.get(
                'meeting-html-%s' % self.meeting.id,
                ''
            )
        )

        self.agenda.save()

        self.assertEqual(None, cache.get('meeting-html-%s' % self.meeting.id))
        response = self.request_schedule_by_date()
        self.assertTrue(
            'Test Meeting' in cache.get(
                'meeting-html-%s' % self.meeting.id,
                ''
            )
        )
