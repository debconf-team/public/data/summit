# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test.client import Client
from django.conf import settings

from model_mommy import mommy as factory

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Track,
    Room,
    Lead,
    Participant,
    Agenda,
)


class VirtualSummitTestCase(djangotest.TestCase):
    """
    Tests for the virtual meeting display
    """

    c = Client()

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(
            Summit,
            name='uds-virt',
            virtual_summit=True,
            date_start=now,
            date_end=now + week,
        )

        self.slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+(2*one_hour),
            type='open',
            summit=self.summit
        )
        self.track = factory.make_one(Track, summit=self.summit)
        self.room = factory.make_one(
            Room,
            summit=self.summit,
            type='open',
            irc_channel='test-channel',
        )

        self.meeting1 = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting-non-virt',
            private=False,
            requires_dial_in=False,
            spec_url='',
            pad_url='http://pad.com/1'
        )
        self.meeting1.tracks = [self.track]

        self.user1 = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_superuser=False,
        )
        self.user1.set_password('password')
        self.user1.save()
        self.attendee1 = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user1,
            start_utc=now,
            end_utc=now+week
        )

        self.agenda1 = factory.make_one(
            Agenda,
            slot=self.slot,
            meeting=self.meeting1,
            room=self.room
        )
        self.save_settings()

    def tearDown(self):
        self.client.logout()
        self.restore_settings()

    def save_settings(self):
        self.ORIGINAL_WEBCHAT_URL = settings.WEBCHAT_URL
        
    def restore_settings(self):
        settings.WEBCHAT_URL = self.ORIGINAL_WEBCHAT_URL
        
    def login(self):
        logged_in = self.c.login(
            username='testuser',
            password='password')
        self.assertTrue(logged_in)

    def view_and_save_edit_meeting_hangout_form(self):
        """
        Tests that a user with proper permissions can save the
        edit_meeting_hangout form
        """
        # View the edit meeting hangout form
        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.c.get(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/edit_hangout.html')

        # Define data to fill our the form
        hangout_url = 'http://hangouturl.com'
        broadcast_url = 'http://broadcasturl.com'

        # Post the form
        post = self.c.post(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            data={
                'broadcast_url': broadcast_url,
                'hangout_url': hangout_url,
            }
        )

        # A successful post should redirect to the meeting page
        response = self.view_virtual_meeting_page()
        redirect_url = reverse(
            'summit.schedule.views.meeting',
            args=rev_args
        )
        self.assertEqual(post.status_code, 302)
        self.assertRedirects(
            post,
            redirect_url,
        )
        
        # Set the agenda timeslot to make the meeting current
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        self.slot.start_utc=now
        self.slot.end_utc=now+one_hour
        self.slot.save()

        redirect_response = self.c.get(redirect_url)

        # Verify that the new data appears on the meeting page
        self.assertEqual(redirect_response.status_code, 200)
        self.assertIn(broadcast_url, redirect_response.content)
        self.assertIn(hangout_url, redirect_response.content)
        self.assertIn('Join the Hangout on Air', redirect_response.content)

    def view_virtual_meeting_page(self):
        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.c.get(
            reverse(
                'summit.schedule.views.meeting',
                args=rev_args
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/virtual_meeting.html')
        self.assertTemplateNotUsed(response, 'schedule/meeting.html')

        return response

    def test_virtual_mtg_true_display_virtual_page(self):
        """
        Tests that a meeting with virtual_summit=True and virtual_meeting=True
        displays the virtual meeting page.
        """

        self.meeting1.virtual_meeting = True
        self.meeting1.save()

        self.view_virtual_meeting_page()

    def test_virtual_mtg_false_display_virtual_page(self):
        """
        Tests that a meeting with virtual_summit=True and virtual_meeting=False
        displays the virtual meeting page.
        """

        self.meeting1.virtual_meeting = False
        self.meeting1.save()
        self.view_virtual_meeting_page()

    def test_lead_has_edit_meeting_hangout_link(self):
        """
        Tests that the track lead for the meeting has the link to add the
        hangout URLs
        """

        self.lead = factory.make_one(
            Lead,
            summit=self.summit,
            track=self.track,
            lead=self.attendee1,
        )

        self.login()
        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.view_virtual_meeting_page()
        self.assertIn(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            response.content
        )
        self.assertIn('Edit Hangout Details', response.content)

    def test_drafter_has_edit_meeting_hangout_link(self):
        """
        Tests that the meeting drafter has the link to add hangout URLs
        """

        self.meeting1.drafter = self.attendee1
        self.meeting1.save()

        self.login()

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.view_virtual_meeting_page()
        self.assertContains(
            response,
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            1
        )
        self.assertContains(response, 'Edit Hangout Details', 1)

    def test_manager_has_edit_meeting_hangout_link(self):
        """
        Tests that a Summit Manager has the link to add hangout URLs
        """

        self.summit.managers.add(self.user1)

        self.login()

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.view_virtual_meeting_page()
        self.assertIn(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            response.content
        )
        self.assertIn('Edit Hangout Details', response.content)

    def test_scheduler_has_edit_meeting_hangout_link(self):
        """
        Tests that a Summit Scheduler has the link to add hangout URLs
        """

        self.summit.schedulers.add(self.user1)

        self.login()

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.view_virtual_meeting_page()
        self.assertIn(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            response.content
        )
        self.assertIn('Edit Hangout Details', response.content)

    def test_regular_user_does_not__have_edit_meeting_hangout_link(self):
        """
        Tests that a regular Summit user is not able to see the edit the
        hangout link
        """

        self.login()

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.view_virtual_meeting_page()
        self.assertContains(
            response,
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            0
        )
        self.assertContains(response, 'Edit Hangout Details', 0)

    def test_non_logged_in_user_does_not_have_edit_meeting_hangout_link(self):
        """
        Tests that a non-logged in user is not able to see the edit the hangout
        link
        """

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.view_virtual_meeting_page()
        self.assertContains(
            response,
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
            0
        )
        self.assertContains(response, 'Edit Hangout Details', 0)

    def test_lead_can_edit_meeting_hangout(self):
        """
        Tests that the track lead for the meeting can add the hangout URLs
        """

        self.lead = factory.make_one(
            Lead,
            summit=self.summit,
            track=self.track,
            lead=self.attendee1,
        )

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='REQUIRED'
        )

        self.login()

        self.view_and_save_edit_meeting_hangout_form()

    def test_drafter_can_edit_meeting_hangout(self):
        """
        Tests that the meeting drafter can add hangout URLs
        """

        self.meeting1.drafter = self.attendee1
        self.meeting1.save()

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='REQUIRED'
        )
        self.login()

        self.view_and_save_edit_meeting_hangout_form()

    def test_manager_can_edit_meeting_hangout(self):
        """
        Tests that a Summit Manager can add hangout URLs
        """

        self.summit.managers.add(self.user1)

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='REQUIRED'
        )
        self.login()

        self.view_and_save_edit_meeting_hangout_form()

    def test_scheduler_can_edit_meeting_hangout(self):
        """
        Tests that a Summit Scheduler can add hangout URLs
        """

        self.summit.schedulers.add(self.user1)

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='REQUIRED'
        )
        self.login()

        self.view_and_save_edit_meeting_hangout_form()

    def test_regular_user_can_not_edit_meeting_hangout(self):
        """
        Tests that a regular Summit user is not able to edit the hangout
        links
        """

        self.login()

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.c.get(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
        )
        self.assertEqual(response.status_code, 302)
        redirect_args = [self.summit.name, ]
        redirect_url = reverse(
            'summit.schedule.views.summit',
            args=redirect_args
        )
        self.assertEqual(
            response['Location'],
            'http://testserver' + redirect_url,
        )

    def test_non_logged_in_user_can_not_edit_meeting_hangout(self):
        """
        Tests that a non-logged in user is not able to edit the hangout
        links
        """

        rev_args = [self.summit.name, self.meeting1.id, self.meeting1.name]
        response = self.c.get(
            reverse(
                'summit.schedule.views.edit_meeting_hangout',
                args=rev_args
            ),
        )
        self.assertEqual(response.status_code, 302)
        redirect_url = reverse(
            'summit.schedule.views.edit_meeting_hangout',
            args=rev_args
        )
        self.assertEqual(
            response['Location'],
            'http://testserver/openid/login?next=' + redirect_url,
        )

    def test_webchat(self):
        """
        Tests that the webchat client is displayed on the virtual meeting page
        with the proper room
        """
        settings.WEBCHAT_URL = 'http://test.com/?channel=%(channel_name)s&foo=bar'
        response = self.view_virtual_meeting_page()
        self.assertIn(
            'http://test.com/?channel=' +
            self.room.irc_channel +
            '&foo=bar',
            response.content
        )

    def test_hangout_broadcast_url(self):
        """
        Tests that the hangout broadcast is displayed on the page
        """

        self.meeting1.broadcast_url = 'http://broadcasturl.com'
        self.meeting1.save()

        response = self.view_virtual_meeting_page()
        self.assertIn(self.meeting1.broadcast_url, response.content)

    def test_participant_essential_join_url(self):
        """
        Tests that a participant who is essential is shown the link to join
        the hangout
        """

        self.meeting1.hangout_url = 'http://hangouturl.com'
        self.meeting1.save()

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='REQUIRED'
        )

        self.login()

        # Set the agenda timeslot to make the meeting current
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        self.slot.start_utc=now
        self.slot.end_utc=now+one_hour
        self.slot.save()

        response = self.view_virtual_meeting_page()
        self.assertIn(self.meeting1.hangout_url, response.content)
        self.assertIn('Join the Hangout on Air', response.content)

    def test_interested_participant_no_join_url(self):
        """
        Tests that a non-essential participant is not shown the link to join
        the hangout
        """

        self.meeting1.hangout_url = 'http://hangouturl.com'
        self.meeting1.save()

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='INTERESTED'
        )

        self.login()

        response = self.view_virtual_meeting_page()
        self.assertNotIn(self.meeting1.hangout_url, response.content)
        self.assertNotIn('Join the Hangout on Air', response.content)

    def test_attending_participant_no_join_url(self):
        """
        Tests that a non-essential (attending) participant is not shown
        the link to join the hangout
        """

        self.meeting1.hangout_url = 'http://hangouturl.com'
        self.meeting1.save()

        self.participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee1,
            participation='ATTENDING'
        )

        self.login()

        response = self.view_virtual_meeting_page()
        self.assertNotIn(self.meeting1.hangout_url, response.content)
        self.assertNotIn('Join the Hangout on Air', response.content)

    def test_non_participant_no_join_url(self):
        """
        Tests that a participant not attending the meeting is not shown the
        link to join the hangout
        """

        self.meeting1.hangout_url = 'http://hangouturl.com'
        self.meeting1.save()

        self.login()

        response = self.view_virtual_meeting_page()
        self.assertNotIn(self.meeting1.hangout_url, response.content)
        self.assertNotIn('Join the Hangout on Air', response.content)

    def test_not_logged_in_no_join_url(self):
        """
        Tests that a non-essential participant is not shown the link to join
        the hangout
        """

        self.meeting1.hangout_url = 'http://hangouturl.com'
        self.meeting1.save()

        response = self.view_virtual_meeting_page()
        self.assertNotIn(self.meeting1.hangout_url, response.content)
        self.assertNotIn('Join the Hangout on Air', response.content)

    def test_etherpad(self):
        """
        Tests that the etherpad client is displayed on the virtual meeting page
        """

        response = self.view_virtual_meeting_page()
        self.assertIn(self.meeting1.pad_url, response.content)
