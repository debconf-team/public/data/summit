# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Meeting,
    Room,
    Attendee,
    Participant,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class MeetingSearchTestCase(djangotest.TestCase):
    """
    This will test different options for deleting a meeting from
    the front end UI.
    """

    form_html = '<form id="site_search_form"'

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        self.summit = factory.make_one(
            Summit,
            name='uds-test',
            date_start=now,
            date_end=now+one_hour
        )
        self.slot = factory.make_one(
            Slot,
            summit=self.summit,
            start_utc=now,
            end_utc=now+one_hour
        )
        self.room = factory.make_one(Room)

    def tearDown(self):
        self.client.logout()

    def test_searchform_exists_on_summit_view(self):
        ''' Search form should appear on the summit view page '''
        response = self.client.get(
            reverse(
                'summit.schedule.views.summit',
                args=[self.summit.name]
            )
        )
        self.assertContains(response, self.form_html, 1)

    def test_searchform_exists_on_daily_view(self):
        ''' Search form should appear on the list view page '''
        response = self.client.get(
            reverse(
                'summit.schedule.views.daily_schedule',
                args=[self.summit.name, '2012-10-10']
            )
        )
        self.assertContains(response, self.form_html, 1)

    def test_searchform_exists_on_today_view(self):
        ''' Search form should appear on the today view page '''
        response = self.client.get(
            reverse(
                'summit.schedule.views.today_view',
                args=[self.summit.name]
            )
        )
        self.assertContains(response, self.form_html, 1)

    def test_searchform_exists_on_meeting_view(self):
        ''' Search form should appear on the meeting view page '''
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            requires_dial_in=False,
            private=False
        )

        response = self.client.get(
            reverse(
                'summit.schedule.views.meeting',
                args=[self.summit.name, meeting.id, meeting.name]
            )
        )
        self.assertContains(response, self.form_html, 1)

    def test_searchform_not_exists_on_index(self):
        '''
        Search form should NOT appear on the index page because
        there isn't a summit selected
        '''
        response = self.client.get(reverse('summit.common.views.index'))
        self.assertContains(response, self.form_html, 0)

    def test_meeting_in_search_results(self):
        '''
        Meeting should appear in search results page if the query
        matches the name or title
        '''
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test-name',
            title='Test Title',
            requires_dial_in=False,
            private=False
        )

        # Attempt to match 'name' against 'test-name' in Meeting.name
        response = self.client.get(
            reverse(
                'summit.schedule.views.search',
                args=[self.summit.name]
            ),
            {'q': 'name'}
        )
        self.assertContains(response, self.form_html, 1)
        self.assertContains(
            response,
            '<a href="%s">Test Title</a>' % meeting.get_meeting_page_url(),
            1
        )

        # Attempt to match 'title' against 'Test Title' in Meeting.title
        response = self.client.get(
            reverse(
                'summit.schedule.views.search',
                args=[self.summit.name]
            ),
            {'q': 'title'}
        )
        self.assertContains(response, self.form_html, 1)
        self.assertContains(
            response,
            '<a href="%s">Test Title</a>' % meeting.get_meeting_page_url(),
            1
        )

    def test_meeting_not_in_search_results(self):
        '''
        Meetings for one summit should NOT appear when searching
        a different summit
        '''
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)

        # Create a second summit
        other_summit = factory.make_one(
            Summit,
            name='uds-other',
            date_start=now,
            date_end=now+one_hour
        )

        meeting = factory.make_one(
            Meeting,
            summit=other_summit,
            name='test-name',
            title='Test Title',
            requires_dial_in=False,
            private=False
        )

        # Attempt to match 'test' against 'test-name' or 'Test Title'
        # should return 0 results because the the meeting is in
        # other_summit, not self.summit
        response = self.client.get(
            reverse(
                'summit.schedule.views.search',
                args=[self.summit.name]
            ),
            {'q': 'test'}
        )
        self.assertContains(response, self.form_html, 1)
        self.assertContains(
            response,
            '<a href="%s">Test Title</a>' % meeting.get_meeting_page_url(),
            0
        )

        # Same test, but searching the correct other_summit should return
        # 1 result
        response = self.client.get(
            reverse(
                'summit.schedule.views.search',
                args=[other_summit.name]
            ),
            {'q': 'test'}
        )
        self.assertContains(response, self.form_html, 1)
        self.assertContains(
            response,
            '<a href="%s">Test Title</a>' % meeting.get_meeting_page_url(),
            1
        )

    def test_private_meeting_not_shown(self):
        '''
        Private meetings should not be in the results if the attendee
        is not attending it
        '''
        now = datetime.datetime.now()
        week = datetime.timedelta(days=7)
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test-name',
            title='Test Title',
            requires_dial_in=False,
            private=True
        )
        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True
        )
        user.set_password('password')
        user.save()
        attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=user,
            start_utc=now,
            end_utc=now+week
        )
        participant = factory.make_one(
            Participant,
            meeting=meeting,
            attendee=attendee,
            participation='ATTENDING',
            from_launchpad=False
        )

        # Attempt to match 'name' against 'test-name' in Meeting.name
        response = self.client.get(
            reverse(
                'summit.schedule.views.search',
                args=[self.summit.name]
            ),
            {'q': 'test'}
        )
        self.assertContains(response, self.form_html, 1)
        self.assertContains(
            response,
            '<a href="%s">Test Title</a>' % meeting.get_meeting_page_url(),
            0
        )

    def test_private_meeting_with_attendee_shown(self):
        '''
        Private meetings should be in the results if the attendee
        is attending it
        '''
        now = datetime.datetime.now()
        week = datetime.timedelta(days=7)
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test-name',
            title='Test Title',
            requires_dial_in=False,
            private=True
        )
        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True
        )
        user.set_password('password')
        user.save()
        attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=user,
            start_utc=now,
            end_utc=now+week
        )
        participant = factory.make_one(
            Participant,
            meeting=meeting,
            attendee=attendee,
            participation='ATTENDING',
            from_launchpad=False
        )

        logged_in = self.client.login(username='testuser', password='password')
        self.assertTrue(logged_in)

        # Attempt to match 'name' against 'test-name' in Meeting.name
        response = self.client.get(
            reverse(
                'summit.schedule.views.search',
                args=[self.summit.name]
            ),
            {'q': 'test'}
        )
        self.assertContains(response, self.form_html, 1)
        self.assertContains(
            response,
            '<a href="%s">Test Title</a>' % meeting.get_meeting_page_url(),
            1
        )
