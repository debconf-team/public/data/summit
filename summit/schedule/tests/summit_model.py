# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest

from model_mommy import mommy as factory
from summit.schedule.fields import NameField
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test.client import Client

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Agenda,
    SummitSprint,
)

from summit.common import launchpad

from summit.schedule.tests.launchpad_export_node import LaunchpadExportNode

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class SummitModelTestCase(djangotest.TestCase):

    c = Client()

    def get_basic_launchpad_response(self):
        elem = LaunchpadExportNode()
        elem.add_child("attendees", LaunchpadExportNode())
        elem.add_child("unscheduled", LaunchpadExportNode())
        return elem

    def make_one_future_slot(self, summit=None):
        if summit is None:
            summit = factory.make_one(Summit)
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(hours=1)
        return factory.make_one(
            Slot,
            summit=summit,
            start=now+one_hour,
            end=now+one_hour+one_hour,
            type='open'
        )

    def test_update_meeting_skips_no_name(self):
        summit = factory.make_one(Summit)
        elem = LaunchpadExportNode()
        meeting = summit.update_meeting_from_launchpad(elem)
        self.assertEqual(None, meeting)
        self.assertEqual(0, Meeting.objects.all().count())

    def test_update_meeting_trims_name(self):
        summit = factory.make_one(Summit)
        elem = LaunchpadExportNode(name="very " * 20 + "longname", id="42")
        meeting = summit.update_meeting_from_launchpad(elem)
        # Names are truncated at 100 chars.
        expected_name = "very " * 20
        self.assertEqual(expected_name, meeting.name)
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact=expected_name
            ).count()
        )

    def test_update_meeting_accepts_existing_meeting(self):
        summit = factory.make_one(Summit)
        name = "meeting-name"
        title = "other-title"
        elem = LaunchpadExportNode(name=name, id="42")
        summit.meeting_set.create(name=name, title=title)
        meeting = summit.update_meeting_from_launchpad(elem)
        self.assertEqual(name, meeting.name)
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact=name,
                title__exact=title
            ).count()
        )

    def test_update_from_launchpad_response_empty(self):
        summit = factory.make_one(Summit)
        elem = self.get_basic_launchpad_response()
        meetings = summit.update_from_launchpad_response(elem)
        self.assertEqual(set(), meetings)
        self.assertEqual(0, Meeting.objects.all().count())
        self.assertEqual(0, Attendee.objects.all().count())

    def test_update_from_launchpad_response_handles_no_name(self):
        summit = factory.make_one(Summit)
        elem = self.get_basic_launchpad_response()
        meeting_node = LaunchpadExportNode(name=None, id="42")
        elem.find("unscheduled").add_child("meeting", meeting_node)
        meetings = summit.update_from_launchpad_response(elem)
        self.assertEqual(set(), meetings)

    def test_launchpad_sprint_import_urls_uses_default(self):
        summit = factory.make_one(Summit, name='test-sprint')
        self.assertEqual(
            ['https://launchpad.net/sprints/test-sprint/+temp-meeting-export'],
            summit.launchpad_sprint_import_urls()
        )

    def test_launchpad_sprint_import_url_uses_one_summit_sprint(self):
        import_url = 'http://example.com/test'
        summit = factory.make_one(Summit)
        factory.make_one(SummitSprint, summit=summit, import_url=import_url)
        self.assertEqual([import_url], summit.launchpad_sprint_import_urls())

    def test_launchpad_sprint_import_url_uses_two_summit_sprint(self):
        import_url1 = 'http://example.com/test1'
        import_url2 = 'http://example.com/test2'
        summit = factory.make_one(Summit)
        factory.make_one(SummitSprint, summit=summit, import_url=import_url1)
        factory.make_one(SummitSprint, summit=summit, import_url=import_url2)
        self.assertEqual(
            sorted(
                [import_url1, import_url2]
            ),
            sorted(
                summit.launchpad_sprint_import_urls()
            )
        )

    def test_update_from_launchpad_gets_info_for_all_import_urls(self):
        import_url1 = 'http://example.com/test1'
        import_url2 = 'http://example.com/test2'
        summit = factory.make_one(Summit)
        factory.make_one(SummitSprint, summit=summit, import_url=import_url1)
        factory.make_one(SummitSprint, summit=summit, import_url=import_url2)
        called_urls = []

        def get_sprint_info(url):
            called_urls.append(url)
            return self.get_basic_launchpad_response()

        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertEqual(
            sorted(
                [import_url1, import_url2]
            ),
            sorted(called_urls)
        )

    def test_update_from_launchpad_does_the_update(self):
        summit = factory.make_one(Summit)

        def get_sprint_info(url):
            elem = self.get_basic_launchpad_response()
            meeting_node = LaunchpadExportNode(name="foo", id="42")
            elem.find("unscheduled").add_child("meeting", meeting_node)
            return elem

        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertEqual(1, summit.meeting_set.all().count())

    def test_update_from_launchpad_does_renames(self):
        summit = factory.make_one(Summit)
        meeting = summit.meeting_set.create(
            name="name",
            launchpad_blueprint_id="42"
        )

        def get_sprint_info(url):
            elem = self.get_basic_launchpad_response()
            meeting_node = LaunchpadExportNode(name="other", id="42")
            elem.find("unscheduled").add_child("meeting", meeting_node)
            return elem

        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        # Since both had blueprint id 42, it should just update
        # the existing meeting
        self.assertEqual(
            0,
            summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact="other"
            ).count()
        )

    def test_update_from_launchpad_deletes_missing_unscheduled_meetings(self):
        summit = factory.make_one(Summit)
        meeting = summit.meeting_set.create(
            spec_url='test_url',
            name="name",
            launchpad_blueprint_id="42"
        )

        def get_sprint_info(url):
            elem = self.get_basic_launchpad_response()
            meeting_node = LaunchpadExportNode(name="other")
            elem.find("unscheduled").add_child("meeting", meeting_node)
            return elem

        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            0,
            summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved='REMOVED'
            ).count()
        )

    def test_update_from_launchpad_deletes_missing_scheduled_meetings(self):
        summit = factory.make_one(Summit)
        meeting = summit.meeting_set.create(
            spec_url='test_url',
            name="name",
            launchpad_blueprint_id="42"
        )
        room = factory.make_one(Room, summit=summit)
        slot = factory.make_one(Slot)
        agenda = meeting.agenda_set.create(room=room, slot=slot, auto=True)

        def get_sprint_info(url):
            elem = self.get_basic_launchpad_response()
            meeting_node = LaunchpadExportNode(name="other")
            elem.find("unscheduled").add_child("meeting", meeting_node)
            return elem
        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            0,
            summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved='REMOVED'
            ).count()
        )

    def test_update_from_launchpad_doesnt_delete_meetings_with_spec_url(self):
        summit = factory.make_one(Summit)
        meeting = summit.meeting_set.create(
            spec_url='http://example.com/foo',
            name="name",
            launchpad_blueprint_id="42"
        )

        def get_sprint_info(url):
            elem = self.get_basic_launchpad_response()
            meeting_node = LaunchpadExportNode(name="other", id="43")
            elem.find("unscheduled").add_child("meeting", meeting_node)
            return elem
        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )

    def test_update_from_launchpad_wont_delete_meetings_without_spec_url(self):
        summit = factory.make_one(Summit)
        meeting = summit.meeting_set.create(
            spec_url='',
            name="name",
            launchpad_blueprint_id="42"
        )

        def get_sprint_info(url):
            elem = self.get_basic_launchpad_response()
            meeting_node = LaunchpadExportNode(name="other", id="43")
            elem.find("unscheduled").add_child("meeting", meeting_node)
            return elem

        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertEqual(
            1,
            summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )

    def test_update_from_launchpad_updates_last_update(self):
        old_now = datetime.datetime.utcnow() - datetime.timedelta(days=1)
        summit = factory.make_one(Summit, last_update=old_now)

        def get_sprint_info(url):
            return self.get_basic_launchpad_response()

        summit._get_sprint_info_from_launchpad = get_sprint_info
        summit.update_from_launchpad()
        self.assertTrue(old_now < summit.last_update)

    def test_reschedule_does_nothing_on_empty_schedule(self):
        summit = factory.make_one(Summit)
        summit.reschedule()

    def test_reschedule_removes_missing_participants(self):
        summit = factory.make_one(Summit)
        meeting1 = factory.make_one(
            Meeting,
            summit=summit,
            requires_dial_in=False,
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            summit=summit,
            requires_dial_in=False,
            private=False
        )
        room1 = factory.make_one(Room, summit=summit)
        room2 = factory.make_one(Room, summit=summit)
        slot = self.make_one_future_slot(summit=summit)
        attendee = factory.make_one(
            Attendee,
            summit=summit,
            start_utc=slot.start_utc,
            end_utc=slot.end_utc
        )
        meeting1.participant_set.create(
            attendee=attendee,
            participation='REQUIRED'
        )
        meeting2.participant_set.create(
            attendee=attendee,
            participation='REQUIRED'
        )
        factory.make_one(
            Agenda,
            meeting=meeting1,
            room=room1,
            slot=slot,
            auto=True
        )
        factory.make_one(
            Agenda,
            meeting=meeting2,
            room=room2,
            slot=slot,
            auto=True
        )
        summit.reschedule()
        self.assertEqual(1, slot.agenda_set.all().count())

    def test_reschedule_removes_unavailable_participants(self):
        summit = factory.make_one(Summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            requires_dial_in=False,
            private=False
        )
        room = factory.make_one(Room, summit=summit)
        slot = self.make_one_future_slot(summit=summit)
        attendee = factory.make_one(
            Attendee,
            summit=summit,
            start_utc=slot.end_utc,
            end_utc=slot.end_utc+datetime.timedelta(hours=1)
        )
        meeting.participant_set.create(
            attendee=attendee,
            participation='REQUIRED'
        )
        factory.make_one(
            Agenda,
            meeting=meeting,
            room=room,
            slot=slot,
            auto=True
        )
        summit.reschedule()
        self.assertEqual(0, slot.agenda_set.all().count())

    def test_reschedule_removes_insufficient_slots(self):
        summit = factory.make_one(Summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            slots=2,
            requires_dial_in=False,
            private=False
        )
        room = factory.make_one(Room, summit=summit)
        slot = self.make_one_future_slot(summit=summit)
        factory.make_one(
            Agenda,
            meeting=meeting,
            room=room,
            slot=slot,
            auto=True
        )
        summit.reschedule()
        self.assertEqual(0, slot.agenda_set.all().count())

    def test_reschedule_leaves_old_slots(self):
        summit = factory.make_one(Summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            slots=2,
            requires_dial_in=False,
            private=False
        )
        room = factory.make_one(Room, summit=summit)
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(hours=1)
        slot = factory.make_one(
            Slot,
            summit=summit,
            start=now-one_hour,
            end=now,
            type='open'
        )
        factory.make_one(
            Agenda,
            meeting=meeting,
            room=room,
            slot=slot,
            auto=True
        )
        summit.reschedule()
        self.assertEqual(1, slot.agenda_set.all().count())

    def test_reschedule_leaves_manually_scheduled(self):
        summit = factory.make_one(Summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            slots=2,
            requires_dial_in=False,
            private=False
        )
        room = factory.make_one(Room, summit=summit)
        slot = self.make_one_future_slot(summit=summit)
        factory.make_one(
            Agenda,
            meeting=meeting,
            room=room,
            slot=slot,
            auto=False
        )
        summit.reschedule()
        self.assertEqual(1, slot.agenda_set.all().count())

    def test_update_from_launchpad_adds_attendees(self):
        def mock_set_openid(user, force=False):
            self.assertEquals(user.username, 'testuser')
            mock_set_openid.called = True
            return True
        mock_set_openid.called = False
        launchpad.set_user_openid = mock_set_openid

        summit = factory.make_one(Summit)
        attendee_node = LaunchpadExportNode(name='testuser')
        summit.update_attendee_from_launchpad(attendee_node)

        self.assertEqual(
            1,
            summit.attendee_set.filter(
                user__username__exact="testuser"
            ).count()
        )
        self.assertEqual(
            1,
            summit.attendee_set.filter(from_launchpad="True").count()
        )
        self.assertTrue(mock_set_openid.called)

    def test_non_launchpad_summit_attendee_update_from_launchpad(self):
        """
        Tests that a user who has registered in Summit does not have thier
        data overridden
        """
        now = datetime.datetime.utcnow()
        delta = datetime.timedelta(days=1)
        end_summit = now + 3*delta
        start_summit_string = now.strftime("%Y-%m-%dT%H:%M:%SZ")
        end_summit_string = end_summit.strftime("%Y-%m-%dT%H:%M:%SZ")
        end_user = now + 2*delta
        user = factory.make_one(User, username="testuser")
        summit = factory.make_one(
            Summit,
            timezone="UTC",
            date_start=now,
            date_end=end_summit,
        )

        attendee = factory.make_one(
            Attendee,
            user=user,
            summit=summit,
            start_utc=summit.date_start,
            end_utc=end_user,
            from_launchpad=False,
        )
        attendee_node = LaunchpadExportNode(
            name=user.username,
            start=start_summit_string,
            end=end_summit_string,
        )
        summit.update_attendee_from_launchpad(attendee_node)

        attendee = Attendee.objects.get(user=user)

        self.assertEqual(
            attendee.start_utc.replace(second=0, microsecond=0),
            now.replace(second=0, microsecond=0),
        )
        self.assertEqual(
            attendee.end_utc.replace(second=0, microsecond=0),
            end_user.replace(second=0, microsecond=0),
        )

    def test_summit_attendee_update_from_launchpad(self):
        """
        Tests that a user from launchpad is updated with their registration
        time from launchpad
        """
        now = datetime.datetime.utcnow()
        delta = datetime.timedelta(days=1)
        end_summit = now + 3*delta
        start_summit_string = now.strftime("%Y-%m-%dT%H:%M:%SZ")
        end_summit_string = end_summit.strftime("%Y-%m-%dT%H:%M:%SZ")
        end_user = now + 2*delta
        user = factory.make_one(User, username="testuser")
        summit = factory.make_one(
            Summit,
            timezone="UTC",
            date_start=now,
            date_end=end_summit,
        )

        attendee = factory.make_one(
            Attendee,
            user=user,
            summit=summit,
            start_utc=summit.date_start,
            end_utc=end_user,
            from_launchpad=True,
        )
        attendee_node = LaunchpadExportNode(
            name=user.username,
            start=start_summit_string,
            end=end_summit_string,
        )

        summit.update_attendee_from_launchpad(attendee_node)

        attendee = Attendee.objects.get(user=user)

        self.assertEqual(
            attendee.start_utc.replace(second=0),
            now.replace(second=0, microsecond=0),
        )
        self.assertEqual(
            attendee.end_utc.replace(second=0, microsecond=0),
            end_summit.replace(second=0, microsecond=0),
        )

    def load_summit_page(self, summit):
        rev_args = [summit.name, ]
        response = self.c.get(
            reverse(
                'summit.schedule.views.summit',
                args=rev_args,
            )
        )
        self.assertEqual(response.status_code, 200)

        return response

    def create_summit(self):
        summit = factory.make_one(
            Summit,
            name="uds-test",
            date_start="2013-05-09",
            date_end="2013-05-10",
        )

        return summit

    def test_summit_page_displays_lp_link(self):
        """
        Tests that the summit main page displays the Launchpad link when
        one an import_url exists
        """
        summit = self.create_summit()
        import_url = "http://launchpad"
        factory.make_one(SummitSprint, summit=summit, import_url=import_url)
        self.assertEqual([import_url], summit.launchpad_sprint_import_urls())
        response = self.load_summit_page(summit)
        self.assertContains(response, "Launchpad information")

    def test_summit_page_does_not_index_displays_lp_link(self):
        """
        Tests that the summit main page does not display the Launchpad link
        when one an import_url does not exist
        """
        summit = self.create_summit()
        response = self.load_summit_page(summit)
        self.assertNotContains(response, "Launchpad information")

    def test_summit_page_displays_social_media(self):
        """
        Tests that the summit main page displays the social media links when
        social_media=True
        """
        summit = self.create_summit()
        summit.social_media=True
        summit.save()
        response = self.load_summit_page(summit)
        self.assertContains(response, "http://www.reddit.com/submit")

    def test_summit_page_does_not_display_social_media(self):
        """
        Tests that the summit main page does not display the social media
        links when social_media=False
        """
        summit = self.create_summit()
        summit.social_media=False
        summit.save()
        response = self.load_summit_page(summit)
        self.assertNotContains(response, "http://www.reddit.com/submit")
