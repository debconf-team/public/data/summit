# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Agenda,
    Participant,
)

site_root = getattr(settings, 'SITE_ROOT', 'http://summit.ubuntu.com')

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class ICalTestCase(djangotest.TestCase):

    def test_ical_meeting_without_name(self):
        """ Tests that ical doesn't break for nameless meetings"""
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        summit = factory.make_one(Summit, name='uds-test')
        summit.save()
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )
        slot.save()

        room = factory.make_one(Room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='',
            private=False
        )
        meeting.name = ''
        models.Model.save(meeting)
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        self.assertEquals(
            meeting.meeting_page_url,
            '/uds-test/meeting/%s/-/' % meeting.id
        )

        response = self.client.get('/uds-test.ical')
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            'URL:%s/uds-test/meeting/%s/-/\n' % (site_root, meeting.id),
            1
        )

    def test_ical_meeting_name_with_period(self):
        """ Tests that ical doesn't break for nameless meetings"""
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        summit = factory.make_one(Summit, name='uds-test')
        summit.save()
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )
        slot.save()

        room = factory.make_one(Room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='test.name',
            private=False
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        self.assertEquals(
            meeting.meeting_page_url,
            '/uds-test/meeting/%s/test.name/' % meeting.id
        )

        response = self.client.get('/uds-test.ical')
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            'URL:%s/uds-test/meeting/%s/test.name/' % (site_root, meeting.id),
            1
        )

    def test_ical_meeting_multiline_description(self):
        """ Tests that ical put spaces before multi-line descriptions"""
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        summit = factory.make_one(Summit, name='uds-test')
        summit.save()
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )
        slot.save()

        room = factory.make_one(Room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='test.name',
            description="Test\r\nDescription",
            private=False
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        self.assertEquals(
            meeting.meeting_page_url,
            '/uds-test/meeting/%s/test.name/' % meeting.id
        )

        response = self.client.get('/uds-test.ical')
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'DESCRIPTION:Test\NDescription', 1)

    def test_private_ical(self):
        """ Tests that private icals contain private meetings """
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=7)
        summit = factory.make_one(Summit, name='uds-test')
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=summit
        )

        room = factory.make_one(Room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=summit,
            name='test.name',
            private=True
        )
        agenda = factory.make_one(
            Agenda,
            slot=slot,
            meeting=meeting,
            room=room
        )

        self.assertEquals(
            meeting.meeting_page_url,
            '/uds-test/meeting/%s/test.name/' % meeting.id
        )

        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User'
        )
        attendee = factory.make_one(
            Attendee,
            summit=summit,
            user=user,
            start_utc=now,
            end_utc=now+week
        )
        participant = factory.make_one(
            Participant,
            attendee=attendee,
            meeting=meeting
        )

        response = self.client.get(
            '/uds-test/participant/my_schedule_%s.ical' % attendee.secret_key
        )
        self.assertEquals(response.status_code, 200)
        self.assertContains(
            response,
            'URL:%s/uds-test/meeting/%s/test.name/' % (site_root, meeting.id),
            1
        )
