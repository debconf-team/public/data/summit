# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Meeting,
    Track,
    Room,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class ReverseUrlLookupTestCase(djangotest.TestCase):

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        self.summit = factory.make_one(Summit, name='uds-test')
        self.summit.save()
        self.slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=self.summit
        )
        self.slot.save()

    def tearDown(self):
        pass

    def test_meeting_name_with_period(self):
        '''
        Testing a meeting with a . in the name
        '''
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test.meeting',
            private=False
        )

        rev_args = ['uds-test', meeting.id, 'test.meeting']
        reverse_url = reverse('summit.schedule.views.meeting', args=rev_args)
        self.assertEquals(
            reverse_url,
            '/uds-test/meeting/%s/test.meeting/' % meeting.id
        )

    def test_room_name_with_period(self):
        '''
        Test the URL for a room name with a . in the name for both the website
        and the ical
        '''
        room = factory.make_one(Room, summit=self.summit, name='test.room')

        rev_args = ['uds-test', 'test.room']
        reverse_url = reverse('summit.schedule.views.by_room', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/test.room/')

        reverse_url = reverse('summit.schedule.views.room_ical', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/room/test.room.ical')

    def test_track_name_with_period(self):
        '''
        Test the URL for a track name with a . in the name for both the website
        and the ical
        '''
        track = factory.make_one(Track, summit=self.summit, slug='test.track')

        rev_args = ['uds-test', 'test.track']
        reverse_url = reverse('summit.schedule.views.by_track', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/track/test.track/')

        reverse_url = reverse(
            'summit.schedule.views.track_ical',
            args=rev_args
        )
        self.assertEquals(reverse_url, '/uds-test/track/test.track.ical')

    def test_participant_name_with_period(self):
        '''
        Test the URL for a user with a . in their name for the website and
        the iCal
        '''
        user = factory.make_one(User, username='test.user')

        rev_args = ['uds-test', 'test.user']
        reverse_url = reverse('summit.schedule.views.user_ical', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/participant/test.user.ical')

    def test_meeting_name_with_percent(self):
        '''
        Test the URL for a meeting with a % in the name
        '''
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test.meeting',
            private=False
        )

        rev_args = ['uds-test', meeting.id, 'test%meeting']
        reverse_url = reverse('summit.schedule.views.meeting', args=rev_args)
        self.assertEquals(
            reverse_url,
            '/uds-test/meeting/%s/test%%meeting/' % meeting.id
        )

    def test_meeting_name_with_plus_sign(self):
        meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='test.meeting',
            private=False
        )
        rev_args = ['uds-test', meeting.id, 'test+meeting']
        reverse_url = reverse('summit.schedule.views.meeting', args=rev_args)
        self.assertEquals(
            reverse_url,
            '/uds-test/meeting/%s/test+meeting/' % meeting.id
        )

    def test_room_name_with_percent(self):
        '''
        Test the URL for a room with a % in the name for both the website
        and the iCal
        '''
        room = factory.make_one(Room, summit=self.summit, name='test.room')

        rev_args = ['uds-test', 'test%room']
        reverse_url = reverse('summit.schedule.views.by_room', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/test%room/')

        reverse_url = reverse('summit.schedule.views.room_ical', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/room/test%room.ical')

    def test_room_name_with_plus_sign(self):
        room = factory.make_one(Room, summit=self.summit, name='test.room')

        rev_args = ['uds-test', 'test+room']
        reverse_url = reverse('summit.schedule.views.by_room', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/test+room/')

        reverse_url = reverse('summit.schedule.views.room_ical', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/room/test+room.ical')

    def test_track_name_with_percent(self):
        '''
        Test the URL for a track with a % in the name for both the website
        and the iCal
        '''
        track = factory.make_one(Track, summit=self.summit, slug='test.track')

        rev_args = ['uds-test', 'test%track']
        reverse_url = reverse('summit.schedule.views.by_track', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/track/test%track/')

        reverse_url = reverse(
            'summit.schedule.views.track_ical',
            args=rev_args
        )
        self.assertEquals(reverse_url, '/uds-test/track/test%track.ical')

    def test_track_name_with_plus_sign(self):
        track = factory.make_one(Track, summit=self.summit, slug='test.track')

        rev_args = ['uds-test', 'test+track']
        reverse_url = reverse('summit.schedule.views.by_track', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/track/test+track/')

        reverse_url = reverse(
            'summit.schedule.views.track_ical',
            args=rev_args
        )
        self.assertEquals(reverse_url, '/uds-test/track/test+track.ical')

    def test_participant_name_with_percent(self):
        '''
        Test the iCal URL for a participant with a % in the name.
        '''
        user = factory.make_one(User, username='test.user')

        rev_args = ['uds-test', 'test%user']
        reverse_url = reverse('summit.schedule.views.user_ical', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/participant/test%user.ical')

    def test_participant_name_with_plus_sign(self):
        user = factory.make_one(User, username='test.user')

        rev_args = ['uds-test', 'test+user']
        reverse_url = reverse('summit.schedule.views.user_ical', args=rev_args)
        self.assertEquals(reverse_url, '/uds-test/participant/test+user.ical')

    def test_all_meetings_url(self):
        rev_args = ['uds-test']
        response = self.client.get(reverse('all_meetings', args=rev_args))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/all_meetings.html')
