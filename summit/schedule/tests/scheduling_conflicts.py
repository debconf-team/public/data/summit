# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Agenda,
    Participant,
)


# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class SchedulingConflictsTestCase(djangotest.TestCase):

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(Summit, name='uds-test')
        self.slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='open',
            summit=self.summit)

        self.room1 = factory.make_one(Room, summit=self.summit)
        self.meeting1 = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting1',
            requires_dial_in=False
        )
        self.agenda1 = factory.make_one(
            Agenda,
            slot=self.slot,
            meeting=self.meeting1,
            room=self.room1
        )

        self.room2 = factory.make_one(Room, summit=self.summit)
        self.meeting2 = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting2',
            requires_dial_in=False
        )

        self.user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User'
        )
        self.attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user,
            start_utc=now,
            end_utc=now+week
        )

    def tearDown(self):
        pass

    def assertRaises(self, exception_type, function, args):
        try:
            function(*args)
            raise AssertionError(
                'Callable failed to raise exception %s' % exception_type
            )
        except exception_type, e:
            return True

    def test_meeting_check_schedule_no_conflict(self):
        '''Checks the Meeting model's check_schedule'''
        missing = self.meeting1.check_schedule(self.slot, self.room1)
        self.assertEquals(len(missing), 0)

        missing = self.meeting2.check_schedule(self.slot, self.room2)
        self.assertEquals(len(missing), 0)

    def test_meeting_check_room_conflict(self):
        '''Checks that two meetings will not be scheduled in the same room
            at the same time
        '''
        missing = self.meeting1.check_schedule(self.slot, self.room1)
        self.assertEquals(len(missing), 0)

        self.assertRaises(
            Meeting.SchedulingError,
            Meeting.check_schedule,
            (self.meeting2,
             self.slot,
             self.room1)
        )

    def test_meeting_check_schedule_participant_conflict(self):
        '''
        Checks that a two meetings requiring the same attendee will mark that
        user as missing.
        '''
        participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee,
            participation='REQUIRED'
        )
        missing = self.meeting1.check_schedule(self.slot, self.room1)
        self.assertEquals(len(missing), 0)

        participant1 = Participant.objects.create(
            meeting=self.meeting2,
            attendee=self.attendee,
            participation='REQUIRED'
        )
        missing = self.meeting2.check_schedule(self.slot, self.room2)
        self.assertEquals(len(missing), 1)
