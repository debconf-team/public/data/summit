# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Track,
    Room,
    Agenda,
    Participant,
)

from summit.schedule.tests.launchpad_export_node import LaunchpadExportNode

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class MeetingModelTestCase(djangotest.TestCase):
    """Tests for the Meeting model."""

    def setUp(self):
        self.summit = factory.make_one(Summit)
        self.meeting = self.summit.meeting_set.create(
            spec_url='test_url',
            name="name",
            launchpad_blueprint_id="42",
        )

    def test_meetings_can_not_be_scheduled_in_closed_slots(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour,
            type='closed')

        agenda = factory.make_one(Agenda, slot=slot)
        meeting = factory.make_one(
            Meeting,
            requires_dial_in=False,
            private=False
        )

        # XXX check_schedule should only be checking the schedule, not
        # checking and modifying the schedule.
        # XXX check_schedule's parameters should be an just an agenda object,
        # not the agenda object's attributes.
        self.assertRaises(
            Meeting.SchedulingError,
            meeting.check_schedule, agenda.slot, agenda.room)

    def test_participants_are_in_another_meeting(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        slot = factory.make_one(
            Slot,
            start_utc=now,
            end_utc=now+one_hour)
        room1 = factory.make_one(Room)
        room2 = factory.make_one(Room)

        meeting1 = factory.make_one(
            Meeting,
            summit=slot.summit,
            requires_dial_in=False,
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            summit=slot.summit,
            requires_dial_in=False,
            private=False
        )

        attendee = factory.make_one(Attendee)
        factory.make_one(
            Participant,
            meeting=meeting1,
            attendee=attendee,
            participation='REQUIRED'
        )
        factory.make_one(
            Participant,
            meeting=meeting2,
            attendee=attendee,
            participation='REQUIRED'
        )

        factory.make_one(Agenda, meeting=meeting1, slot=slot, room=room1)
        agenda = factory.make_one(
            Agenda,
            meeting=meeting2,
            slot=slot,
            room=room2
        )

        missing = meeting2.check_schedule(agenda.slot, agenda.room)
        self.assertEqual([attendee.name], [a.name for a in missing])

    def make_open_slot(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+one_hour+one_hour,
            type='open')
        return slot

    def test_check_schedule_errors_on_no_dial_in(self):
        slot = self.make_open_slot()
        room = factory.make_one(
            Room,
            has_dial_in=False,
            summit=slot.summit,
            name="testroom"
        )
        meeting = factory.make_one(
            Meeting,
            requires_dial_in=True,
            summit=slot.summit,
            name="testmeeting",
            private=False
        )
        try:
            meeting.check_schedule(slot, room)
        except meeting.SchedulingError, e:
            self.assertEqual("Room has no dial-in capability", e.message)
            return
        self.fail("SchedulingError not thrown")

    def make_two_adjacent_slots(self):
        summit = factory.make_one(Summit, timezone='utc')
        now = datetime.datetime(2011, 9, 8, 12, 00)
        one_hour = datetime.timedelta(0, 3600)
        slot1 = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+one_hour+one_hour,
            type='open', summit=summit
        )
        slot2 = factory.make_one(
            Slot,
            start_utc=now+one_hour+one_hour,
            end_utc=now+one_hour+one_hour+one_hour,
            type='open', summit=summit
        )
        return slot1, slot2

    def test_check_schedule_errors_on_same_track_in_previous_slot(self):
        slot1, slot2 = self.make_two_adjacent_slots()
        room = factory.make_one(
            Room,
            summit=slot1.summit,
            name="testroom"
        )
        track = factory.make_one(
            Track,
            summit=slot1.summit,
            title="testtrack",
            allow_adjacent_sessions=False
        )
        track2 = factory.make_one(
            Track,
            summit=slot1.summit,
            title="adjacenttrack",
            allow_adjacent_sessions=True
        )
        meeting1 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting1",
            type='blueprint',
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting2",
            type='blueprint',
            private=False
        )
        meeting1.tracks = [track, track2]
        meeting2.tracks = [track]
        meeting1.agenda_set.create(room=room, slot=slot1)
        try:
            meeting2.check_schedule(slot2, room)
        except meeting2.SchedulingError, e:
            self.assertEqual("Same track in the previous slot", e.message)
            return
        self.fail("SchedulingError not thrown")

    def test_check_schedule_errors_on_same_track_in_next_slot(self):
        slot1, slot2 = self.make_two_adjacent_slots()
        room = factory.make_one(Room, summit=slot1.summit, name="testroom")
        track = factory.make_one(
            Track,
            summit=slot1.summit,
            title="testtrack",
            allow_adjacent_sessions=False
        )
        meeting1 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting1",
            type='blueprint',
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting2",
            type='blueprint',
            private=False
        )
        meeting1.tracks = [track]
        meeting2.tracks = [track]
        meeting1.agenda_set.create(room=room, slot=slot2)
        try:
            meeting2.check_schedule(slot1, room)
        except meeting2.SchedulingError, e:
            self.assertEqual("Same track in the next slot", e.message)
            return
        self.fail("SchedulingError not thrown")

    def test_check_schedule_no_error_on_different_track(self):
        slot1, slot2 = self.make_two_adjacent_slots()
        room = factory.make_one(Room, summit=slot1.summit, name="testroom")
        track = factory.make_one(
            Track,
            summit=slot1.summit,
            title="testtrack",
            allow_adjacent_sessions=False
        )
        other_track = factory.make_one(
            Track,
            summit=slot1.summit,
            title="othertesttrack",
            allow_adjacent_sessions=False
        )
        meeting1 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting1",
            type='blueprint',
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting2",
            type='blueprint',
            private=False
        )
        meeting1.tracks = [track]
        meeting2.tracks = [other_track]
        meeting1.agenda_set.create(room=room, slot=slot2)
        meeting2.check_schedule(slot1, room)

    def test_check_schedule_no_error_on_same_track_for_plenaries(self):
        slot1, slot2 = self.make_two_adjacent_slots()
        room = factory.make_one(Room, summit=slot1.summit, name="testroom")
        track = factory.make_one(
            Track,
            summit=slot1.summit,
            title="testtrack",
            allow_adjacent_sessions=False
        )
        meeting1 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting1",
            type='blueprint',
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting2",
            type='plenary',
            private=False
        )
        meeting1.tracks = [track]
        meeting2.tracks = [track]
        meeting1.agenda_set.create(room=room, slot=slot2)
        meeting2.check_schedule(slot1, room)

    def test_check_schedule_no_error_same_track_ajdacent_sessions_allowed(
        self
    ):
        slot1, slot2 = self.make_two_adjacent_slots()
        room = factory.make_one(Room, summit=slot1.summit, name="testroom")
        track = factory.make_one(
            Track,
            summit=slot1.summit,
            title="adjacenttrack",
            allow_adjacent_sessions=True
        )
        meeting1 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting1",
            type='blueprint',
            private=False
        )
        meeting2 = factory.make_one(
            Meeting,
            requires_dial_in=False,
            summit=slot1.summit,
            name="testmeeting2",
            type='blueprint',
            private=False
        )
        meeting1.tracks = [track]
        meeting2.tracks = [track]
        meeting1.agenda_set.create(room=room, slot=slot2)
        meeting2.check_schedule(slot1, room)

    def test_try_schedule_into_refuses_room_without_dial_in(self):
        slot = self.make_open_slot()
        room = factory.make_one(
            Room,
            has_dial_in=False,
            summit=slot.summit,
            name="testroom"
        )
        meeting = factory.make_one(
            Meeting,
            requires_dial_in=True,
            summit=slot.summit,
            name="testmeeting",
            private=False
        )

        self.assertEqual(False, meeting.try_schedule_into([room]))
        self.assertEqual(0, meeting.agenda_set.all().count())

    def test_try_schedule_into_allows_room_with_dial_in(self):
        slot = self.make_open_slot()
        room = factory.make_one(
            Room,
            has_dial_in=True,
            summit=slot.summit,
            name="testroom"
        )
        meeting = factory.make_one(
            Meeting,
            requires_dial_in=True,
            summit=slot.summit,
            name="testmeeting",
            private=False
        )

        self.assertEqual(True, meeting.try_schedule_into([room]))
        self.assertEqual(1, meeting.agenda_set.all().count())

    def test_link_to_pad_with_pad_url_set(self):
        url = 'http://pad.com/url'
        meeting = factory.make_one(Meeting, pad_url=url, private=False)
        self.assertEqual(url, meeting.link_to_pad)

    def get_pad_host(self):
        summit_name = 'testsummit'
        summit = factory.make_one(Summit, name=summit_name)
        return getattr(summit, 'etherpad', 'http://pad.ubuntu.com/')

    def test_link_to_pad_with_pad_url_unset(self):
        summit_name = 'testsummit'
        summit = factory.make_one(Summit, name=summit_name)
        name = 'testmeeting'
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name=name,
            summit=summit,
            private=False
        )
        pad_host = self.get_pad_host()
        url = pad_host + summit_name + '-' + name
        self.assertEqual(url, meeting.link_to_pad)

    def test_link_to_pad_with_plus_in_meeting_name(self):
        summit_name = 'testsummit'
        summit = factory.make_one(Summit, name=summit_name)
        name = 'test+meeting'
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name=name,
            summit=summit,
            private=False
        )
        pad_host = self.get_pad_host()
        url = pad_host + summit_name + '-' + name.replace("+", "-")
        self.assertEqual(url, meeting.link_to_pad)

    def test_edit_link_to_pad_with_pad_url_set(self):
        url = 'http://pad.com/url'
        meeting = factory.make_one(Meeting, pad_url=url, private=False)
        self.assertEqual(url, meeting.edit_link_to_pad)

    def test_edit_link_to_pad_with_pad_url_unset(self):
        summit_name = 'testsummit'
        summit = factory.make_one(Summit, name=summit_name)
        name = 'testmeeting'
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name=name,
            summit=summit,
            private=False
        )
        pad_host = self.get_pad_host()
        url = pad_host + "ep/pad/view/" + summit_name + '-' + name + "/latest"
        self.assertEqual(url, meeting.edit_link_to_pad)

    def test_edit_link_to_pad_with_plus_in_meeting_name(self):
        summit_name = 'testsummit'
        summit = factory.make_one(Summit, name=summit_name)
        name = 'test+meeting'
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name=name,
            summit=summit,
            private=False
        )
        pad_host = self.get_pad_host()
        url = (
            pad_host
            + "ep/pad/view/"
            + summit_name
            + '-'
            + name.replace("+", "-")
            + "/latest"
        )
        self.assertEqual(url, meeting.edit_link_to_pad)

    def test_update_from_launchpad_sets_status(self):
        summit = factory.make_one(Summit, timezone='utc', name='test-summit')
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        status = "Discussion"
        name = meeting.name
        elem = LaunchpadExportNode(status=status, name=name)
        meeting.update_from_launchpad(elem)
        self.assertEqual(status, meeting.status)

    def test_update_from_launchpad_sets_priority(self):
        summit = factory.make_one(Summit, timezone='utc', name='test-summit')
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        priority = 70
        name = meeting.name
        elem = LaunchpadExportNode(priority=priority, name=name)
        meeting.update_from_launchpad(elem)
        self.assertEqual(priority, meeting.priority)

    def test_update_from_launchpad_sets_wiki_url(self):
        summit = factory.make_one(Summit, timezone='utc', name='test-summit')
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        wiki_url = "http://example.com/somespec"
        name = meeting.name
        elem = LaunchpadExportNode(specurl=wiki_url, name=name)
        meeting.update_from_launchpad(elem)
        self.assertEqual(wiki_url, meeting.wiki_url)

    def get_person_node(self, username, name=None, required=False):
        if name:
            elem = LaunchpadExportNode(name=name)
        else:
            elem = LaunchpadExportNode()
        required_map = {True: "True", False: "False"}
        elem.add_child(
            "person",
            LaunchpadExportNode(
                name=username,
                required=required_map[required]
            )
        )
        return elem

    def make_one_future_slot(self, summit=None):
        if summit is None:
            summit = factory.make_one(Summit)
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(hours=1)
        slot = factory.make_one(
            Slot,
            summit=summit,
            start=now+one_hour,
            end=now+one_hour+one_hour,
            type='open'
        )
        return slot

    def make_summit_only(self):
        today = datetime.datetime.utcnow()
        one_week = datetime.timedelta(days=5)
        summit = factory.make_one(Summit, date_start=today, date_end=today+one_week)
        slot = self.make_one_future_slot(summit=summit)
        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_superuser=False,
        )
        return summit

    def make_summit_and_attendee(self):
        today = datetime.datetime.utcnow()
        one_week = datetime.timedelta(days=5)
        summit = factory.make_one(Summit, date_start=today, date_end=today+one_week)
        slot = self.make_one_future_slot(summit=summit)
        user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_superuser=False,
        )
        attendee = factory.make_one(
            Attendee,
            summit=summit,
            user=user,
            start_utc=slot.start_utc,
            end_utc=slot.end_utc
        )
        return summit, attendee

    def test_update_from_launchpad_creates_attendee(self):
        summit = self.make_summit_only()
        self.assertEquals(summit.attendee_set.filter(user__username='testuser').count(), 0)
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        meeting_name = meeting.name
        elem = self.get_person_node(
            'testuser',
            meeting_name,
            required=False,
        )
        meeting.update_from_launchpad(elem)
        self.assertEquals(summit.attendee_set.filter(user__username='testuser').count(), 1)

    def test_update_from_launchpad_creates_drafter(self):
        summit = self.make_summit_only()
        self.assertEquals(summit.attendee_set.filter(user__username='testuser').count(), 0)
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        meeting_name = meeting.name
        elem = LaunchpadExportNode(
            name=meeting_name,
            drafter='testuser',
        )
        meeting.update_from_launchpad(elem)
        self.assertEquals(summit.attendee_set.filter(user__username='testuser').count(), 1)
        self.assertEquals(meeting.drafter.user.username, 'testuser')

    def test_update_from_launchpad_adds_participant(self):
        summit, attendee = self.make_summit_and_attendee()
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        meeting_name = meeting.name
        elem = self.get_person_node(
            attendee.user.username,
            meeting_name,
            required=False,
        )
        meeting.update_from_launchpad(elem)
        participant = meeting.participant_set.get()
        self.assertEqual(attendee, participant.attendee)
        self.assertEqual('ATTENDING', participant.participation)

    def test_update_from_launchpad_sets_participant_essential(self):
        summit, attendee = self.make_summit_and_attendee()
        meeting = factory.make_one(
            Meeting,
            pad_url=None,
            name='test-meeting',
            summit=summit,
            private=False
        )
        meeting_name = meeting.name
        username = attendee.user.username
        required = True

        elem = self.get_person_node(
            username,
            meeting_name,
            required,
        )
        meeting.update_from_launchpad(elem)
        participant = meeting.participant_set.get()
        self.assertEqual('INTERESTED', participant.participation)

    def test_update_from_launchpad_sets_from_launchpad(self):
        summit, attendee = self.make_summit_and_attendee()
        meeting = summit.meeting_set.create()
        elem = self.get_person_node(attendee.user.username, meeting.name)
        meeting.update_from_launchpad(elem)
        participant = meeting.participant_set.get()
        self.assertEqual(True, participant.from_launchpad)

    def test_update_from_launchpad_removes_from_launchpad_unsubscribed(self):
        summit, attendee = self.make_summit_and_attendee()
        meeting = summit.meeting_set.create()
        elem = self.get_person_node(attendee.user.username, meeting.name)
        slot = self.make_one_future_slot(summit=summit)
        otheruser = factory.make_one(User, username="otheruser")
        otherattendee = summit.attendee_set.create(
            user=otheruser,
            start_utc=slot.start_utc,
            end_utc=slot.end_utc
        )
        meeting.participant_set.create(
            attendee=otherattendee,
            from_launchpad=True,
        )
        meeting.update_from_launchpad(elem)
        usernames = [
            p.attendee.user.username for p in meeting.participant_set.all()
        ]
        self.assertEqual(["testuser"], usernames)

    def test_update_from_launchpad_obsolete_blueprint(self):
        """
        Tests that a blueprint being marked obsolete is changed to REMOVED
        and not scheduled
        """
        room = factory.make_one(Room, summit=self.summit)
        slot = factory.make_one(Slot)
        agenda = self.meeting.agenda_set.create(room=room, slot=slot, auto=True)
        status = "APPROVED"
        elem = LaunchpadExportNode(status=status, name=self.meeting.name)
        self.meeting.update_from_launchpad(elem)
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        status = "OBSOLETE"
        elem = LaunchpadExportNode(status=status, name=self.meeting.name)
        self.meeting.update_from_launchpad(elem)
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            0,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved='REMOVED'
            ).count()
        )

    def test_update_from_launchpad_superseded_blueprint(self):
        """
        Tests that a blueprint that was approved is removed from agenda
        after being marked superseded
        """
        room = factory.make_one(Room, summit=self.summit)
        slot = factory.make_one(Slot)
        agenda = self.meeting.agenda_set.create(room=room, slot=slot, auto=True)
        status = "APPROVED"
        elem = LaunchpadExportNode(status=status, name=self.meeting.name)
        self.meeting.update_from_launchpad(elem)
        meeting = self.summit.meeting_set.filter(name__exact="name").values()
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            0,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved__in=['PENDING', 'APPROVED', 'DECLINED']
            ).count()
        )
        status = "SUPERSEDED"
        elem = LaunchpadExportNode(status=status, name=self.meeting.name)
        self.meeting.update_from_launchpad(elem)
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            0,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved='REMOVED'
            ).count()
        )
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved__in=['PENDING', 'APPROVED', 'DECLINED']
            ).count()
        )

    def test_update_from_launchpad_superseded_now_new_blueprint(self):
        """
        Test that a blueprint that was marked superseded and is now new
        is available for scheduling
        """
        status = "SUPERSEDED"
        elem = LaunchpadExportNode(status=status, name=self.meeting.name)
        self.meeting.update_from_launchpad(elem)
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved__in=['PENDING', 'APPROVED', 'DECLINED']
            ).count()
        )
        status = "NEW"
        elem = LaunchpadExportNode(status=status, name=self.meeting.name)
        self.meeting.update_from_launchpad(elem)
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).count()
        )
        self.assertEqual(
            1,
            self.summit.meeting_set.filter(
                name__exact="name"
            ).exclude(
                approved='REMOVED'
            ).count()
        )
        
    def test_meetings_current(self):
        # A meeting without an agenda is never current
        self.assertFalse(self.meeting.current)
        
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        # A meeting in a future slot is not current
        slot = factory.make_one(
            Slot,
            start_utc=now + one_hour,
            end_utc=now + one_hour + one_hour,
            type='open')
        agenda = factory.make_one(Agenda, meeting=self.meeting, slot=slot)
        self.assertFalse(self.meeting.current)

        # A meeting in a past slot is not current
        slot.start_utc = now - one_hour - one_hour
        slot.end_utc = now - one_hour
        slot.save()
        self.assertFalse(self.meeting.current)

        # A meeting happening now is current
        slot.start_utc = now
        slot.end_utc = now + one_hour
        slot.save()
        self.assertTrue(self.meeting.current)

    def test_irc_log_urls(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)

        slot = factory.make_one(
            Slot,
            start_utc=now + one_hour,
            end_utc=now + one_hour + one_hour,
            type='open',
        )
        room = factory.make_one(
            Room,
            summit=self.summit,
            irc_channel='test-irc',
        )
        agenda = factory.make_one(Agenda, meeting=self.meeting, slot=slot, room=room)
        
        check_url = 'http://irclogs.ubuntu.com/%s/%%23%s.html#t%s' % (
            agenda.slot.start_utc.strftime('%Y/%m/%d'),
            agenda.room.irc_channel,
            agenda.slot.start_utc.strftime('%H:%M'),
        )
        self.assertEqual(check_url, agenda.irc_log)
        self.assertEqual(check_url, self.meeting.irc_logs[0])

