# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Permission

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Agenda,
    Participant,
    Lead,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class DeleteMeetingTestCase(djangotest.TestCase):
    """
    This will test different options for deleting a meeting from
    the front end UI.
    """

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        self.summit = factory.make_one(
            Summit,
            name='uds-test',
            date_start=now,
            date_end=now+one_hour
        )
        self.summit.save()
        slot = factory.make_one(
            Slot,
            summit=self.summit,
            start_utc=now,
            end_utc=now+one_hour
        )
        room = factory.make_one(Room)

        self.meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            requires_dial_in=False,
            private=False
        )

        attendee1 = factory.make_one(Attendee)
        factory.make_one(Participant, meeting=self.meeting, attendee=attendee1)

        factory.make_one(Agenda, meeting=self.meeting, slot=slot, room=room)

        self.user = factory.make_one(
            User,
            username='testuser',
            is_active=True,
            is_superuser=False
        )
        self.user.set_password('password')
        self.user.save()

        self.attendee = factory.make_one(
            Attendee,
            user=self.user,
            summit=self.summit
        )

    def tearDown(self):
        self.client.logout()
        self.user.user_permissions.all().delete()

    def login(self):
        logged_in = self.client.login(username='testuser', password='password')
        self.assertTrue(logged_in)

    def test_track_lead_cant_delete_meeting(self):
        self.lead = factory.make_one(
            Lead,
            lead=self.attendee,
            summit=self.summit
        )
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.delete_meeting',
                args=(self.summit, self.meeting.id, self.meeting.name)
            )
        )
        self.assertRedirects(
            response,
            reverse(
                'summit.schedule.views.summit',
                args=(self.summit.name,)
            ),
            status_code=302,
            target_status_code=200
        )

    def test_user_cant_delete_meeting(self):
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.delete_meeting',
                args=(self.summit, self.meeting.id, self.meeting.name)
            )
        )
        self.assertRedirects(
            response,
            reverse(
                'summit.schedule.views.summit',
                args=(self.summit.name,)
            ),
            status_code=302,
            target_status_code=200
        )

    def test_managers_cant_delete_meeting(self):
        self.summit.managers.add(self.user)
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.delete_meeting',
                args=(self.summit, self.meeting.id, self.meeting.name)
            )
        )
        self.assertRedirects(
            response,
            reverse(
                'summit.schedule.views.summit',
                args=(self.summit.name,)
            ),
            status_code=302,
            target_status_code=200
        )

    def test_schedulers_can_delete_meeting(self):
        self.summit.schedulers.add(self.user)
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.delete_meeting',
                args=(self.summit, self.meeting.id, self.meeting.name)
            )
        )
        self.assertRedirects(
            response,
            reverse(
                'summit.schedule.views.delete_confirmed',
                args=(self.summit.name,)
            ),
            status_code=302,
            target_status_code=200
        )

    def test_change_agenda_can_delete_meeting(self):
        """
        Testing if a user who is assigned can_change_agenda
        in django admin can delete a meeting
        """
        change_agenda = Permission.objects.get(codename='change_agenda')
        self.user.user_permissions.add(change_agenda)
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.delete_meeting',
                args=(self.summit, self.meeting.id, self.meeting.name)
            )
        )
        self.assertRedirects(
            response,
            reverse(
                'summit.schedule.views.delete_confirmed',
                args=(self.summit.name,)
            ),
            status_code=302,
            target_status_code=200
        )
