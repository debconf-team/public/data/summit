# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.core.cache import cache

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Track,
    Room,
    Agenda,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class RenderScheduleTestCase(djangotest.TestCase):

    def setUp(self):
        now = datetime.datetime.utcnow()
        if now.hour >= 22:
            now.replace(hour=now.hour-2)
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(Summit, name='uds-test', timezone='UTC')
        self.slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+(2*one_hour),
            type='open',
            summit=self.summit
        )

        self.track1 = factory.make_one(
            Track,
            slug='test_slug',
            summit=self.summit
        )
        self.room1 = factory.make_one(
            Room,
            summit=self.summit,
            name='room1',
            start_utc=now,
            end_utc=now+week,
        )
        self.meeting1 = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting1',
            private=False,
            requires_dial_in=False,
        )
        self.agenda1 = factory.make_one(
            Agenda,
            slot=self.slot,
            meeting=self.meeting1,
            room=self.room1
        )

        self.user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User'
        )
        self.attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user,
            start_utc=now,
            end_utc=now+week
        )

    def tearDown(self):
        # Cached requests cause render.py to return old data.
        # We will clear the cache
        if hasattr(cache, 'clear'):
            cache.clear()
        # Older django didn't have .clear, but locmem cache did have ._cull
        elif hasattr(cache, '_cull'):
            cache._cull()

    def request_schedule(self):
        schedule_args = [self.summit.name, self.agenda1.slot.start_utc.date()]
        schedule_url = reverse(
            'summit.schedule.views.by_date',
            args=schedule_args
        )
        response = self.client.get(schedule_url)
        return response

    def test_percent_in_meeting_name(self):
        self.meeting1.name = 'test%meeting'
        self.meeting1.save()

        response = self.request_schedule()

        self.assertContains(response, 'test%meeting', 1)

    def test_percent_in_meeting_title(self):
        self.meeting1.title = 'test % meeting'
        self.meeting1.save()

        response = self.request_schedule()

        self.assertContains(response, 'test % meeting', 1)

    def test_percent_in_room_title(self):
        self.meeting1.type = 'talk'
        self.meeting1.save()
        self.room1.title = 'test % room'
        self.room1.save()

        response = self.request_schedule()

        # Room title is displayed at top and bottom of the schedule,
        # and also on the meeting div for self.meeting1 which is scheduled
        # for that room
        self.assertContains(response, 'test % room', 4)

    def test_percent_in_meeting_track_title(self):
        self.track1.title = 'test % track'
        self.track1.save()
        self.meeting1.tracks.add(self.track1)

        response = self.request_schedule()

        self.assertContains(response, 'test % track', 1)

    def test_percent_in_meeting_track_slug(self):
        self.track1.slug = 'test%track'
        self.track1.save()
        self.meeting1.tracks.add(self.track1)

        response = self.request_schedule()

        self.assertContains(response, 'test%track', 1)

    def test_specify_rooms_in_schedule(self):
        room2 = factory.make_one(
            Room,
            summit=self.summit,
            name='room2',
            title='Room 2'
        )
        self.room1.title = 'Room 1'
        self.room1.save()
        self.agenda1.delete()

        schedule_args = [self.summit.name, self.agenda1.slot.start_utc.date()]
        schedule_url = reverse(
            'summit.schedule.views.by_date',
            args=schedule_args
        )

        response = self.client.get(schedule_url)
        self.assertContains(response, self.room1.title, 2)
        self.assertContains(response, room2.title, 2)

        response = self.client.get(schedule_url + '?rooms=room1')
        self.assertContains(response, self.room1.title, 2)
        self.assertContains(response, room2.title, 0)

        response = self.client.get(schedule_url + '?rooms=room2')
        self.assertContains(response, self.room1.title, 0)
        self.assertContains(response, room2.title, 2)

        response = self.client.get(schedule_url + '?rooms=room1,room2')
        self.assertContains(response, self.room1.title, 2)
        self.assertContains(response, room2.title, 2)

        response = self.client.get(schedule_url + '?rooms=unknown1,unknown2')
        self.assertContains(response, self.room1.title, 0)
        self.assertContains(response, room2.title, 0)
