# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.contrib.auth.models import User

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Agenda,
    Participant,
)


# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class AutoSchedulerTestCase(djangotest.TestCase):
    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(Summit, name='uds-test')
        self.slot = factory.make_one(
            Slot,
            start_utc=now+week,
            end_utc=now+week+one_hour,
            type='open',
            summit=self.summit
        )

        self.room1 = factory.make_one(Room, summit=self.summit, type='open')
        self.meeting1 = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting1',
            requires_dial_in=False,
            approved='APPROVED',
            private=False
        )
        self.agenda1 = factory.make_one(
            Agenda,
            slot=self.slot,
            meeting=self.meeting1,
            room=self.room1
        )

        self.room2 = factory.make_one(Room, summit=self.summit, type='open', name='testroom')
        self.meeting2 = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting2',
            requires_dial_in=False,
            approved='APPROVED',
            private=False
        )

        self.user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User'
        )
        self.attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user,
            start_utc=now,
            end_utc=now+week+one_hour
        )

    def tearDown(self):
        pass

    def run_autoschedule(self):
        from django.core.management import execute_from_command_line
        execute_from_command_line(
            argv=['manage.py', 'autoschedule', 'uds-test', '-v', '2']
        )

    def run_reschedule(self):
        from django.core.management import execute_from_command_line
        execute_from_command_line(
            argv=['manage.py', 'reschedule', 'uds-test', '-v', '2']
        )

    def test_required_conflict(self):
        participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee,
            participation='REQUIRED'
        )

        participant2 = Participant.objects.create(
            meeting=self.meeting2,
            attendee=self.attendee,
            participation='REQUIRED'
        )

        self.run_autoschedule()

        self.assertEquals(1, self.meeting1.agenda_set.all().count())
        self.assertEquals(0, self.meeting2.agenda_set.all().count())

    def test_required_and_interested(self):
        participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee,
            participation='REQUIRED'
        )

        participant2 = Participant.objects.create(
            meeting=self.meeting2,
            attendee=self.attendee,
            participation='INTERESTED'
        )

        self.run_autoschedule()

        self.assertEquals(1, self.meeting1.agenda_set.all().count())
        self.assertEquals(1, self.meeting2.agenda_set.all().count())

    def test_interested_and_required(self):
        participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee,
            participation='REQUIRED'
        )

        participant2 = Participant.objects.create(
            meeting=self.meeting2,
            attendee=self.attendee,
            participation='INTERESTED'
        )

        self.run_autoschedule()

        self.assertEquals(1, self.meeting1.agenda_set.all().count())
        self.assertEquals(1, self.meeting2.agenda_set.all().count())

        participant2.participation = 'REQUIRED'
        participant2.save()

        self.run_reschedule()

        # Only one should be un-scheduled, but which one is random
        self.assertEquals(1, self.meeting1.agenda_set.all().count() + self.meeting2.agenda_set.all().count())

    def test_get_participants_by_level(self):
        participant1 = Participant.objects.create(
            meeting=self.meeting1,
            attendee=self.attendee,
            participation='REQUIRED'
        )

        participant2 = Participant.objects.create(
            meeting=self.meeting2,
            attendee=self.attendee,
            participation='INTERESTED'
        )

        self.assertEquals(
            1,
            len(self.meeting1.get_participants_by_level('REQUIRED'))
        )
        self.assertEquals(
            0, len(self.meeting1.get_participants_by_level('INTERESTED'))
        )
        self.assertEquals(
            0,
            len(self.meeting1.get_participants_by_level('ATTENDING'))
        )

        self.assertEquals(
            0,
            len(self.meeting2.get_participants_by_level('REQUIRED'))
        )
        self.assertEquals(
            1,
            len(self.meeting2.get_participants_by_level('INTERESTED'))
        )
        self.assertEquals(
            0,
            len(self.meeting2.get_participants_by_level('ATTENDING'))
        )
