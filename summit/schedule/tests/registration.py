# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django import test as djangotest
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from django.test.client import Client

from model_mommy import mommy as factory

from summit.schedule.models import (
    Summit,
    Attendee,
)


class RegistrationTestCase(djangotest.TestCase):
    """
    Tests for registering to attend a Summit
    """
    c = Client()

    def setUp(self):
        self.now = datetime.datetime.utcnow()
        self.one_hour = datetime.timedelta(0, 3600)
        self.one_day = datetime.timedelta(days=1)
        self.week = datetime.timedelta(days=5)
        self.end_summit = self.now + self.week
        self.summit = factory.make_one(
            Summit,
            name='test-summit',
            title='Test Summit',
            virtual_summit=True,
            date_start=self.now,
            date_end=self.now + self.week,
            timezone='UTC',
        )

        self.user1 = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True,
            is_superuser=False,
        )
        self.user1.set_password('password')
        self.user1.save()

    def create_attendee(self):
        self.attendee1 = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user1,
            start_utc=self.now,
            end_utc=self.now+self.week
        )

    def tearDown(self):
        self.client.logout()

    def login(self):
        logged_in = self.c.login(
            username='testuser',
            password='password')
        self.assertTrue(logged_in)

    def get_attendee(self):
        attendee = Attendee.objects.get(user=self.user1)
        return attendee

    def view_summit_page(self):
        rev_args = [self.summit.name, ]
        response = self.c.get(
            reverse(
                'summit.schedule.views.summit',
                args=rev_args
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/summit.html')

        return response

    def with_summit_registration_times(self):
        """
        Using the start and end of the summit as the registration times
        """
        self.start_date = self.now.strftime("%Y-%m-%d")
        self.start_hour = self.now.strftime("%I")
        self.start_minute = self.now.strftime("%M")
        self.start_period = self.now.strftime("%p")
        self.end_hour = self.end_summit.strftime("%I")
        self.end_minute = self.end_summit.strftime("%M")
        self.end_period = self.end_summit.strftime("%p")
        self.end_date = self.end_summit.strftime("%Y-%m-%d")
        self.edit_registration_form()
        attendee = self.get_attendee()
        self.assertEqual(
            attendee.start_utc.replace(second=0),
            self.now.replace(second=0, microsecond=0),
        )
        self.assertEqual(
            attendee.end_utc.replace(second=0, microsecond=0),
            self.end_summit.replace(second=0, microsecond=0),
        )

    def with_custom_registration_times(self):
        """
        Using custom start and end registration times
        """
        start = self.now + self.one_day
        self.start_date = start.strftime("%Y-%m-%d")
        self.start_hour = self.now.strftime("%I")
        self.start_minute = self.now.strftime("%M")
        self.start_period = self.now.strftime("%p")
        self.end_hour = self.end_summit.strftime("%I")
        self.end_minute = self.end_summit.strftime("%M")
        self.end_period = self.end_summit.strftime("%p")
        self.end_date = self.end_summit.strftime("%Y-%m-%d")
        self.edit_registration_form()
        attendee = self.get_attendee()
        self.assertEqual(
            attendee.start_utc.replace(second=0),
            start.replace(second=0, microsecond=0),
        )
        self.assertEqual(
            attendee.end_utc.replace(second=0, microsecond=0),
            self.end_summit.replace(second=0, microsecond=0),
        )

    def edit_registration_form(self):
        """
        Tests that a user can register for a Summit
        """
        # Define data to fill our the form

        # Post the form
        post = self.c.post(
            reverse(
                'registration',
                args=(self.summit.name,)
            ),
            data={
                'end_utc_0': self.end_date,
                'end_utc_1_0': self.end_hour,
                'end_utc_1_1': self.end_minute,
                'end_utc_1_2': self.end_period,
                'start_utc_1_0': self.start_hour,
                'start_utc_1_1': self.start_minute,
                'start_utc_1_2': self.start_period,
                'start_utc_0': self.start_date,
            }
        )

        # A successful post should redirect to the summit page
        response = reverse(
            'summit.schedule.views.summit',
            args=(self.summit.name,)
        )
        self.assertEqual(post.status_code, 302)
        self.assertRedirects(post, response)
        attendee = self.get_attendee()
        self.assertEqual(attendee.user, self.user1)
        self.assertEqual(attendee.from_launchpad, False)

    def test_non_attendee_registers(self):
        self.login()
        self.assertRaises(Attendee.DoesNotExist, lambda: self.get_attendee())
        rev_args = [self.summit.name, ]
        response = self.c.get(reverse('registration', args=rev_args))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/form.html')
        self.assertIn('Register for ' + self.summit.title, response.content)
        self.with_summit_registration_times()

    def test_attendee_updates_registration(self):
        self.create_attendee()
        self.login()
        self.assertEquals(
            self.user1.username,
            self.get_attendee().user.username,
        )
        rev_args = [self.summit.name, ]
        response = self.c.get(reverse('registration', args=rev_args))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/form.html')
        self.assertIn(
            'Update registration for ' + self.summit.title,
            response.content
        )
        self.with_custom_registration_times()

    def test_update_registration_from_launchpad_true(self):
        """
        update resgistration with from_launchpad=True
        to save from_launchpad=False
        """
        self.create_attendee()
        self.attendee1.from_launchpad = True
        self.attendee1.save()
        self.login()
        self.assertEquals(
            self.user1.username,
            self.get_attendee().user.username,
        )
        self.assertEquals(
            True,
            self.get_attendee().from_launchpad,
        )
        rev_args = [self.summit.name, ]
        response = self.c.get(reverse('registration', args=rev_args))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'schedule/form.html')
        self.assertIn(
            'Update registration for ' + self.summit.title,
            response.content
        )
        self.with_custom_registration_times()

    def test_registered_view(self):
        """
        Test that when a user is already registered for the summit
        that they will see a link to update their registration
        """
        self.create_attendee()
        self.attendee1.from_launchpad = True
        self.attendee1.save()
        self.login()
        rev_args = [self.summit.name, ]
        response = self.view_summit_page()
        self.assertIn(
            reverse(
                'registration',
                args=rev_args
            ),
            response.content
        )
        self.assertIn('Update registration', response.content)

    def test_unregistered_view(self):
        """
        Test that when a user is not registered for the summit
        that they will see a button to register
        """
        self.login()
        rev_args = [self.summit.name, ]
        response = self.view_summit_page()
        self.assertIn(
            reverse(
                'registration',
                args=rev_args
            ),
            response.content
        )
        self.assertIn('Register in Summit', response.content)
