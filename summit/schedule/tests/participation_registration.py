# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Room,
    Participant,
)

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class ParticipationRegistrationTestCase(djangotest.TestCase):

    def setUp(self):
        now = datetime.datetime.utcnow()
        one_hour = datetime.timedelta(0, 3600)
        week = datetime.timedelta(days=5)
        self.summit = factory.make_one(Summit, name='uds-test')
        self.slot = factory.make_one(
            Slot,
            start_utc=now+one_hour,
            end_utc=now+(2*one_hour),
            type='open',
            summit=self.summit
        )

        self.room = factory.make_one(Room, summit=self.summit, type='open')
        self.meeting = factory.make_one(
            Meeting,
            summit=self.summit,
            name='meeting1',
            private=False,
            requires_dial_in=False,
            spec_url=''
        )

        self.user = factory.make_one(
            User,
            username='testuser',
            first_name='Test',
            last_name='User',
            is_active=True
        )
        self.user.set_password('password')
        self.user.save()

        self.attendee = factory.make_one(
            Attendee,
            summit=self.summit,
            user=self.user,
            start_utc=now,
            end_utc=now+week
        )

    def tearDown(self):
        pass

    def login(self):
        logged_in = self.client.login(username='testuser', password='password')
        self.assertTrue(logged_in)

    def test_attend_link(self):
        self.assertEquals(
            0,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.meeting',
                args=('uds-test', self.meeting.id, 'meeting1')
            )
        )
        self.assertContains(response, 'Attend this meeting', 1)
        self.assertContains(response, 'Subscribe to this meeting', 0)
        self.assertContains(response, 'Skip this meeting', 0)

    def test_subscribe_link(self):
        self.assertEquals(
            0,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )
        self.login()
        self.meeting.spec_url = 'http://examplespec.com/test'
        self.meeting.save()
        response = self.client.get(
            reverse(
                'summit.schedule.views.meeting',
                args=('uds-test', self.meeting.id, 'meeting1')
            )
        )
        self.assertContains(response, 'Subscribe to blueprint', 1)
        self.assertContains(
            response,
            'http://examplespec.com/test/+subscribe',
            1
        )
        self.assertContains(response, 'Attend this meeting', 1)
        self.assertContains(response, 'Skip this meeting', 0)

    def test_skip_link(self):
        self.meeting.participant_set.create(
            attendee=self.attendee,
            participation='ATTENDING',
            from_launchpad=False
        )
        self.assertEquals(
            1,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.meeting',
                args=('uds-test', self.meeting.id, 'meeting1')
            )
        )
        self.assertContains(response, 'Skip this meeting', 1)
        self.assertContains(response, 'Subscribe to this meeting', 0)
        self.assertContains(response, 'Attend this meeting', 0)

    def test_add_participation(self):
        self.assertEquals(
            0,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.register',
                args=('uds-test', self.meeting.id, 'meeting1')
            )
        )
        self.assertEquals(
            1,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )

    def test_delete_participation(self):
        self.meeting.participant_set.create(
            attendee=self.attendee,
            participation='ATTENDING',
            from_launchpad=False
        )
        self.assertEquals(
            1,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )
        self.login()
        response = self.client.get(
            reverse(
                'summit.schedule.views.unregister',
                args=('uds-test', self.meeting.id, 'meeting1')
            )
        )
        self.assertEquals(
            0,
            Participant.objects.filter(
                attendee=self.attendee
            ).count()
        )
