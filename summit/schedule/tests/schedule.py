# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
import pytz
from django import test as djangotest
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from model_mommy import mommy as factory
from summit.schedule.fields import NameField

from summit.schedule.models import (
    Summit,
    Slot,
    Attendee,
    Meeting,
    Track,
    Room,
)

from summit.schedule.render import Schedule

from summit.schedule.tests.request_factory import RequestFactory

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class ScheduleTestCase(djangotest.TestCase):

    def get_request(self):
        return RequestFactory().request()

    def get_schedule_from_request(self, date=None):
        request = self.get_request()
        summit = factory.make_one(Summit)
        return Schedule.from_request(request, summit, date=date)

    def test_default_read_only(self):
        schedule = self.get_schedule_from_request()
        self.assertEqual(False, schedule.edit)

    def get_user_with_schedule_permission(self):
        user = factory.make_one(
            User,
            is_active=True,
            is_staff=False,
            is_superuser=False
        )
        user.user_permissions.create(
            codename='change_agenda',
            content_type=factory.make_one(ContentType, app_label='schedule')
        )
        return user

    def get_get_request(self, **kwargs):
        return RequestFactory().get('/someurl/', data=kwargs)

    def get_edit_request(self, **kwargs):
        kwargs['edit'] = True
        return self.get_get_request(**kwargs)

    def test_editable(self):
        user = self.get_user_with_schedule_permission()
        request = self.get_edit_request()
        request.user = user
        summit = factory.make_one(Summit, state='schedule')
        attendee = factory.make_one(Attendee, summit=summit, user=user)
        schedule = Schedule.from_request(request, summit, attendee)
        self.assertEqual(True, schedule.edit)

    def test_read_only_for_public_summit(self):
        user = self.get_user_with_schedule_permission()
        request = self.get_edit_request()
        request.user = user
        summit = factory.make_one(Summit, state='public')
        attendee = factory.make_one(Attendee, summit=summit, user=user)
        schedule = Schedule.from_request(request, summit, attendee)
        self.assertEqual(False, schedule.edit)

    def test_read_only_for_non_edit_request(self):
        user = self.get_user_with_schedule_permission()
        request = self.get_request()
        request.user = user
        summit = factory.make_one(Summit, state='schedule')
        attendee = factory.make_one(Attendee, summit=summit, user=user)
        schedule = Schedule.from_request(request, summit, attendee)
        self.assertEqual(False, schedule.edit)

    def test_read_only_for_unauthenticated_user(self):
        request = self.get_edit_request()
        summit = factory.make_one(Summit, state='schedule')
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(False, schedule.edit)

    def test_read_only_for_user_without_permission(self):
        user = factory.make_one(User, is_active=True, is_superuser=False)
        request = self.get_edit_request()
        request.user = user
        summit = factory.make_one(Summit, state='schedule')
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(False, schedule.edit)

    def test_default_not_personal(self):
        schedule = self.get_schedule_from_request()
        self.assertEqual(False, schedule.personal)

    def test_personal_if_specified_in_get(self):
        request = self.get_get_request(personal=True)
        summit = factory.make_one(Summit)
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(True, schedule.personal)

    def test_date_set_from_date(self):
        date = datetime.date.today()
        schedule = self.get_schedule_from_request(date=date)
        self.assertEqual(date, schedule.date)
        self.assertEqual([date], schedule.dates)

    def test_date_parsed_from_string(self):
        date = datetime.date.today()
        date_str = date.strftime("%Y-%m-%d")
        schedule = self.get_schedule_from_request(date=date_str)
        self.assertEqual(date, schedule.date)
        self.assertEqual([date], schedule.dates)

    def test_dates_set_from_summit_if_not_passed(self):
        schedule = self.get_schedule_from_request()
        self.assertEqual(None, schedule.date)
        self.assertEqual(schedule.summit.dates(), schedule.dates)

    def test_room_set_from_room(self):
        request = self.get_request()
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit)
        schedule = Schedule.from_request(request, summit, room=room)
        self.assertEqual(room, schedule.room)
        self.assertEqual([room], schedule.rooms)

    def test_room_set_from_rooms(self):
        request = self.get_request()
        summit = factory.make_one(Summit)
        rooms = factory.make_many(Room, 2, summit=summit)
        schedule = Schedule.from_request(request, summit, room=rooms)
        self.assertEqual(None, schedule.room)
        self.assertEqual(rooms, schedule.rooms)

    def test_room_set_from_summit_if_not_passed(self):
        request = self.get_request()
        summit = factory.make_one(Summit)
        room1 = factory.make_one(
            Room,
            summit=summit,
            type='open',
            name="room1"
        )
        room2 = factory.make_one(
            Room,
            summit=summit,
            type='open',
            name="room2"
        )
        factory.make_one(Room, summit=summit, type='closed')
        factory.make_one(Room, summit=summit, type='private')
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(None, schedule.room)
        self.assertEqual(
            ["room1", "room2"],
            sorted([r.name for r in schedule.rooms])
        )

    def test_rooms_include_private_if_user_is_staff(self):
        user = factory.make_one(
            User,
            is_active=True,
            is_staff=True,
            is_superuser=False
        )
        request = self.get_request()
        request.user = user
        summit = factory.make_one(Summit)
        room1 = factory.make_one(
            Room,
            summit=summit,
            type='open',
            name="room1"
        )
        room2 = factory.make_one(
            Room,
            summit=summit,
            type='open',
            name="room2"
        )
        factory.make_one(Room, summit=summit, type='closed')
        factory.make_one(
            Room,
            summit=summit,
            type='private',
            name="privateroom"
        )
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(None, schedule.room)
        self.assertEqual(
            ["privateroom", "room1", "room2"],
            sorted([r.name for r in schedule.rooms])
        )

    def test_rooms_include_private_if_show_private(self):
        request = self.get_request()
        summit = factory.make_one(Summit)
        room1 = factory.make_one(
            Room,
            summit=summit,
            type='open',
            name="room1"
        )
        room2 = factory.make_one(
            Room,
            summit=summit,
            type='open',
            name="room2"
        )
        factory.make_one(Room, summit=summit, type='closed')
        factory.make_one(
            Room,
            summit=summit,
            type='private',
            name="privateroom"
        )
        schedule = Schedule.from_request(request, summit, show_private=True)
        self.assertEqual(None, schedule.room)
        self.assertEqual(
            ["privateroom", "room1", "room2"],
            sorted([r.name for r in schedule.rooms])
        )

    def test_track_is_none_by_default(self):
        schedule = self.get_schedule_from_request()
        self.assertEqual(None, schedule.track)

    def test_track_set_from_track(self):
        request = self.get_request()
        summit = factory.make_one(Summit)
        track = factory.make_one(Track, summit=summit)
        schedule = Schedule.from_request(request, summit, track=track)
        self.assertEqual(track, schedule.track)

    def test_nextonly_false_by_default(self):
        schedule = self.get_schedule_from_request()
        self.assertEqual(False, schedule.nextonly)

    def test_nextonly_set_from_get_parameters(self):
        request = self.get_get_request(next=True)
        summit = factory.make_one(Summit)
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(True, schedule.nextonly)

    def test_fakenow_none_by_default(self):
        schedule = self.get_schedule_from_request()
        self.assertEqual(None, schedule.fakenow)

    def test_fakenow_set_from_get_parameters(self):
        summit = factory.make_one(Summit)
        date = datetime.datetime(2011, 7, 8, 19, 12)
        date = pytz.timezone(summit.timezone).localize(date)
        request = self.get_get_request(fakenow=date.strftime("%Y-%m-%d_%H:%M"))
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(date, schedule.fakenow)

    def test_fakenow_set_to_none_if_invalid(self):
        summit = factory.make_one(Summit)
        request = self.get_get_request(fakenow="AAAAA")
        schedule = Schedule.from_request(request, summit)
        self.assertEqual(None, schedule.fakenow)

    def get_schedule(
        self,
        edit=False,
        room=None,
        summit=None,
        dates=None,
        rooms=None
    ):
        request = self.get_request()
        if summit is None:
            summit = factory.make_one(Summit)
        schedule = Schedule(
            request,
            summit,
            edit=edit,
            room=room,
            dates=dates,
            rooms=rooms
        )
        return schedule

    def test_calculate_unscheduled_does_nothing_when_read_only(self):
        schedule = self.get_schedule(edit=False)
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_unscheduled_includes_unscheduled(self):
        schedule = self.get_schedule(edit=True)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            private=False,
            approved='APPROVED'
        )
        schedule.calculate_unscheduled()
        self.assertEqual([meeting], schedule.unscheduled)

    def test_calculate_unscheduled_ignores_scheduled_meetings(self):
        schedule = self.get_schedule(edit=True)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            private=False
        )
        room = factory.make_one(Room, summit=schedule.summit)
        slot = factory.make_one(Slot, summit=schedule.summit)
        meeting.agenda_set.create(room=room, slot=slot)
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_unscheduled_ignores_meetings_in_tracks_not_in_this_room(
        self
    ):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='blueprint',
            private=False
        )
        track = factory.make_one(Track, summit=summit)
        other_track = factory.make_one(Track, summit=summit)
        room.tracks = [track]
        meeting.tracks = [other_track]
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_unscheduled_includes_meetings_without_a_track(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='blueprint',
            private=False,
            approved='APPROVED'
        )
        track = factory.make_one(Track, summit=summit)
        room.tracks = [track]
        schedule.calculate_unscheduled()
        self.assertEqual([meeting], schedule.unscheduled)

    def test_calculate_unscheduled_includes_all_meetings_in_room_without_a_track(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='blueprint',
            private=False,
            approved='APPROVED'
        )
        track = factory.make_one(Track, summit=summit)
        meeting.tracks = [track]
        schedule.calculate_unscheduled()
        self.assertEqual([meeting], schedule.unscheduled)

    def test_calculate_unscheduled_includes_meetings_of_the_right_track(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='blueprint',
            private=False,
            approved='APPROVED'
        )
        track = factory.make_one(Track, summit=summit)
        meeting.tracks = [track]
        room.tracks = [track]
        schedule.calculate_unscheduled()
        self.assertEqual([meeting], schedule.unscheduled)

    def test_calculate_unscheduled_includes_meetings_with_one_right_track(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='blueprint',
            private=False,
            approved='APPROVED'
        )
        track = factory.make_one(Track, summit=summit)
        other_track = factory.make_one(Track, summit=summit)
        meeting.tracks = [track, other_track]
        room.tracks = [track]
        schedule.calculate_unscheduled()
        self.assertEqual([meeting], schedule.unscheduled)

    def test_calculate_unscheduled_ignores_plenaries_in_the_room_view(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='plenary',
            private=False
        )
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_unscheduled_ignores_talks_in_the_room_view(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='talk',
            private=False
        )
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_unscheduled_ignores_specials_in_the_room_view(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='open')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='special',
            private=False
        )
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_unscheduled_shows_plenaries_in_the_plenary_room_view(
        self
    ):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='plenary')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='plenary',
            private=False,
            approved='APPROVED'
        )
        schedule.calculate_unscheduled()
        self.assertEqual([meeting], schedule.unscheduled)

    def test_calculate_unscheduled_ignores_non_plenaries_in_the_plenary_room_view(self):
        summit = factory.make_one(Summit)
        room = factory.make_one(Room, summit=summit, type='plenary')
        schedule = self.get_schedule(edit=True, room=room, summit=summit)
        meeting = factory.make_one(
            Meeting,
            summit=schedule.summit,
            type='blueprint',
            private=False
        )
        schedule.calculate_unscheduled()
        self.assertEqual([], schedule.unscheduled)

    def test_calculate_passes_with_multiple_plenary_rooms_if_editing(self):
        now = datetime.datetime.utcnow()
        if now.hour >= 22:
            now.replace(hour=now.hour-2)
        one_hour = datetime.timedelta(hours=1)
        summit = factory.make_one(
            Summit,
            date_start=now.date(),
            date_end=now.date(),
            timezone='UTC'
        )
        slot = factory.make_one(
            Slot,
            summit=summit,
            type='plenary',
            start_utc=now+(1*one_hour),
            end_utc=now+(2*one_hour)
        )
        room1 = factory.make_one(
            Room,
            summit=summit,
            type='plenary',
            start_utc=now+(1*one_hour),
            end_utc=now+(2*one_hour)
        )
        room2 = factory.make_one(
            Room,
            summit=summit,
            type='plenary',
            start_utc=now+(1*one_hour),
            end_utc=now+(2*one_hour)
        )
        schedule = self.get_schedule(
            edit=True,
            rooms=[room1, room2],
            summit=summit,
            dates=[now.date()]
        )
        schedule.calculate()
        # To avoid test being fragile and arbitrarily dependent on the
        # room order, we check the returned schedule.meetings bit-by-bit.
        self.assertEqual([slot], schedule.meetings.keys())
        self.assertEqual(1, len(set(schedule.meetings[slot])))
