from django.test.simple import DjangoTestSuiteRunner
from settings import LOCAL_APPS


class LocalAppsTestSuiteRunner(DjangoTestSuiteRunner):
    """ Override the default 'test' command to only run local tests. """
    def run_tests(self, test_labels, *args, **kwargs):
        if not test_labels:
            test_labels = LOCAL_APPS

        return super(LocalAppsTestSuiteRunner, self).run_tests(
            test_labels,
            *args,
            **kwargs
        )
