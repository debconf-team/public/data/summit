import os
import sys

from settings import *

SITE_ROOT = 'http://summit.linaro.org'

SITE_ID = 2

# Google Analytics code
GOOGLE_ANALYTICS_CODE = 'UA-30315860-1'

try:
    import linaro_website

    INSTALLED_APPS.append('linaro_website')

    TEMPLATE_CONTEXT_PROCESSORS += (
        "linaro_website.media_processor",
        "linaro_website.popup_check",
    )
    TEMPLATE_DIRS += (
        linaro_website.TEMPLATE_DIR,
    )

    THEME_MEDIA = linaro_website.MEDIA_ROOT
except ImportError:
    if not 'init-summit' in sys.argv:
        print "You will need to run ./manage.py init-summit to make The Summit Scheduler fully work."
    else:
        pass

# Days before the start of the summit when we stop allowing
# track leads to edit the schedule
TRACK_LEAD_SCHEDULE_BLACKOUT = 0
WEBCHAT_URL = 'https://kiwiirc.com/client/irc.freenode.com/%(channel_name)s'
