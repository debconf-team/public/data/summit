# Load initial defaults:
../env/bin/python manage.py loaddata --settings debconf_settings debconf_website/json/defaults.json
# Load 2014 DebConf summit:
../env/bin/python manage.py loaddata --settings debconf_settings debconf_website/json/summit_dc14.json
# Load menus:
../env/bin/python manage.py loaddata --settings debconf_settings debconf_website/json/menu.json

