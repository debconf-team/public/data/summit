# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import models as auth_models
from summit.debconf_website.models.usermodel import UserProfile
from summit.sponsor import models as sponsor_models
import pytz, datetime

__all__ = (
    'Sponsorship',
    'SponsorshipJob',
    'SponsorshipFinancialNeed',
)

class SponsorshipJob(models.Model):
    job = models.CharField(max_length=80, blank=False)
    help = models.CharField(max_length=1000, blank=False)

    def __unicode__(self):
        return self.job

    class Meta:
        app_label = 'debconf_website'
	ordering = ('id',)

class SponsorshipFinancialNeed(models.Model):
    need = models.CharField(max_length=500, blank=False)
    help = models.CharField(max_length=1000, blank=False)

    def __unicode__(self):
        return self.need

    class Meta:
        app_label = 'debconf_website'
        ordering = ('id',)


class Sponsorship(sponsor_models.Sponsorship):
    needs_food = models.BooleanField(
        _("I am applying for food sponsorship for DebConf"),
        default=False
    )
    needs_debcamp = models.BooleanField(
        _("I request food and accommodation sponsorship for DebCamp"),
        default=False
    )
    volunteering = models.BooleanField(
        _('I am primarily applying for sponsorship as a volunteer'),
        default=False
    )
    job = models.ForeignKey(SponsorshipJob,
        verbose_name=_('I am primarily applying for sponsorship as a volunteer'),
        on_delete=models.PROTECT,
        default=1
    )
    use = models.TextField(
        _("Contributions to Debian"),
        blank=True
    )
    benefit = models.TextField(
        _("Contributions to DebConf"),
        blank=True
    )
    rationale = models.TextField(
        _("Why do you request help paying for your costs?"),
        blank=True
    )
    userprofile = models.OneToOneField(UserProfile)

    # FIXME: https://github.com/jakewins/django-money
    costs = models.IntegerField(
        _("Total Travel Costs (EUR)"),
        max_length=6, blank=True, null=True, default=None
    )
    needed = models.IntegerField(
        _("Amount requested (EUR)"),
        max_length=6, blank=True, null=True, default=None
    )
    financial_need = models.ForeignKey(SponsorshipFinancialNeed,
       verbose_name=_("Level of Financial Need"),
       default=1
    )
    roommate = models.CharField(
        _("Requested roommate"),
        max_length=80, blank=True
    )

    got_accommodation = models.CharField("Got Accommodation sponosrship", max_length=2, default="", blank=True)
    got_debcamp = models.CharField("Got DebCamp sponsorship", max_length=2, default="", blank=True)
    got_travel = models.CharField("Got Travel sponsorship", max_length=2, default="", blank=True)

    class Meta:
        app_label = 'debconf_website'

