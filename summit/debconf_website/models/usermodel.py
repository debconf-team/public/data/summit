# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import models as auth_models
from django_countries.fields import CountryField
from debconf_website.storage import OverwriteStorage
from summit.schedule.models.attendeemodel import Attendee
import pytz, datetime

__all__ = (
    'Sex',
    'DebianRole',
    'DebConfRole',
    'TShirtSize',
    'RegistrationLevel',
    'FoodAccommodation',
    'Diet',
    'PromotionCode',
    'UserProfile',

    'user_is_dd',
)

def photo_path_for_user(instance, filename):
    # Pretend everything is a .png file because:
    # - avoid a storage DoS from arbitrary extensions (stored files are
    #   not currently removed, so uploading foo.png then foo.jpg means
    #   both will exist).
    # - forces an image mime type in both the web server and browser
    #   (right now, uploading a valid image named foo.html will get
    #   rendered as html, leading to potential XSS, CSRF).
    #
    # FIXME: if this gets run after clean_* is called, maybe the PIL
    # object could be reused and then this could be done instead:
    #ext = instance.pil.format.lower()
    ext = "png"
    return 'photos/%d.%s' % (instance.user.id, ext)


class Sex(models.Model):
    sex = models.CharField(max_length=20, blank=False)

    def __unicode__(self):
        return self.sex

    class Meta:
	app_label = 'debconf_website'
	verbose_name_plural = 'sexes'
        ordering = ('sex',)

class DebianRole(models.Model):
    role = models.CharField(max_length=80, blank=False)
    visible_in_form = models.BooleanField(default=True)

    def __unicode__(self):
        return self.role

    class Meta:
        app_label = 'debconf_website'
        ordering = ('id',)

class DebConfRole(models.Model):
    role = models.CharField(max_length=80, blank=False)
    visible_in_form = models.BooleanField(default=True)

    def __unicode__(self):
        return self.role

    class Meta:
        app_label = 'debconf_website'
	verbose_name = 'DebConf role'
        ordering = ('id',)

class TShirtSize(models.Model):
    size = models.CharField(max_length=80, blank=False)
    visible_in_form = models.BooleanField(default=True)
    order = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return self.size

    class Meta:
        app_label = 'debconf_website'
	verbose_name = 'T-Shirt size'
        ordering = ('visible_in_form', 'order')

class RegistrationLevel(models.Model):
    level = models.CharField(max_length=80, blank=False)
    fee = models.IntegerField(default=0)
    visible_in_form = models.BooleanField(default=True)

    def __unicode__(self):
        return self.level

    class Meta:
        app_label = 'debconf_website'
        ordering = ('fee',)

class FoodAccommodation(models.Model):
    type = models.CharField(max_length=80, blank=False)
    visible_in_form = models.BooleanField(default=True)
    still_selectable = models.BooleanField(default=True)

    def __unicode__(self):
        return self.type

    class Meta:
        app_label = 'debconf_website'
        ordering = ('id',)

class Diet(models.Model):
    preference = models.CharField(max_length=120, blank=False)
    visible_in_form = models.BooleanField(default=True)

    def __unicode__(self):
        return self.preference

    class Meta:
        app_label = 'debconf_website'
        ordering = ('id',)

class PromotionCode(models.Model):
    code = models.CharField(max_length=12, blank=False)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.code

    class Meta:
        app_label = 'debconf_website'
        ordering = ('active', 'code')


class UserProfile(Attendee):
    " Store profile information about a user "
    unique_together = (("user", "summit"),)

    # Badge information.
    badge_full = models.CharField(_("Full name (for badge)"), max_length=80, blank=False)
    badge_nick = models.CharField(_("Nickname (for badge)"), max_length=20, blank=True)

    # Demographic information.
    sex = models.ForeignKey(Sex, verbose_name=_("Gender (for diversity statistics)"),
                            on_delete=models.PROTECT, default=3)

    # Socialization information.
    public_image = models.ImageField(_("Public image/avatar (optional)"),
                                     upload_to=photo_path_for_user,
                                     storage=OverwriteStorage(),
                                     blank=True)

    # Conference details.
    role_debian = models.ForeignKey(DebianRole,
                                    verbose_name=_('Role in Debian'),
                                    on_delete=models.PROTECT, default=7)
    role_debconf = models.ForeignKey(DebConfRole,
                                     verbose_name=_('Role in DebConf'),
                                     on_delete=models.PROTECT, default=7)
    tshirt_size = models.ForeignKey(TShirtSize,
                                    verbose_name=_('T-shirt size'),
                                    on_delete=models.PROTECT, default=10)
    recording_ok = models.IntegerField(_("OK being photographed or filmed?"),
                                       choices=(
        (1, "Yes in public areas, unless I say otherwise"),
        (0, "No, unless I give my permission"),
                                       ),
                                       max_length=1,
                                       default=1)
    attend = models.BooleanField(_("I want to attend DebConf"),
                                 default=True)
    reconfirm = models.NullBooleanField(_("Reconfirm attendance"),
                                        null=True)
    registration_level = models.ForeignKey(RegistrationLevel,
                                           on_delete=models.PROTECT, default=1)
    donation = models.IntegerField(verbose_name=_("Donation (in EUR)"),
                                   default=0, null=True)
    food_accommodation = models.ForeignKey(FoodAccommodation, verbose_name=_("Food and accommodation options"),
                                           on_delete=models.PROTECT, default=2)

    diet = models.ForeignKey(Diet, verbose_name=_("Dietary standard"),
                                     on_delete=models.PROTECT, default=1)
    daytrip = models.BooleanField(_("Participate in Day Trip"),
                                    default=False, blank=False)
    assassins = models.BooleanField(_("Participate in Assassins game"),
                                    default=False, blank=False)
    child_care = models.BooleanField(
                        _("Interested in information about group child care"),
                        default=False,
                        blank=False)
    subscribe = models.BooleanField(_("Subscribe to conference announcements mailing list"),
                                    default=True, blank=False)

    attend_cwp = models.BooleanField(
        _("Attend Cheese & Wine Party"),
        help_text=('The <a href="/debconf14/meeting/34/cheese-and-wine-party/">DebConf14 '
                   'Cheese & Wine Party</a>, like all of DebConf, is governed '
                   'by the <a href="http://debconf.org/codeofconduct.shtml">DebConf '
                   'Code of Conduct</a>.  By choosing to attend this event, '
                   'you give consent to the DebConf14 team to share your name '
                   'with the event sponsor, Puppet Labs'),
        default=False,
        blank=False
    )

    notes = models.TextField(
        max_length=2047,
        verbose_name="Optional notes for registration (including roommate preferences)",
        null=True,
        blank=True
    )

    # Contact details.
    contact_email = models.CharField(_("Contact email address"),
                                     max_length=80, blank=True)
    public_email = models.CharField(_("Public email address"),
                                    max_length=80, blank=True)
    telephone = models.CharField(_("Contact phone number"),
                                 max_length=40, blank=True)

    address = models.TextField(_("Address"), blank=True)
    city_state = models.CharField(_("City, State/Province (where applicable)"),
                                  max_length=40, blank=True)
    postcode = models.CharField(_("Postal Code"), max_length=40, blank=True)
    country = CountryField(verbose_name=_("Country"), blank=True)
    emergency_name = models.CharField(_("Emergency contact name"),
                                      max_length=40, blank=True)
    emergency_contact = models.CharField(_("Emergency contact phone/email"),
                                         max_length=80, blank=True)
    emergency_note = models.TextField(_("Emergency contact"),
                                      blank=True, max_length=500)

    # Travel details.
    arrival_needs = models.BooleanField(_("Special needs"),
                                        default=False)
    departure_needs = models.BooleanField(_("Special departure transport requirements?"),
                                          default=False)

    # Front Desk details.
    arrived = models.BooleanField(_("Has arrived at DebConf?"),
                                  default=False)
    received_tshirt = models.BooleanField(_("Has received T-shirt?"),
                                          default=False)
    received_bag = models.BooleanField(_("Has received bag?"),
                                       default=False)
    fee_paid = models.BooleanField(_("Registration fee has been paid?"),
                                   default=False)
    can_get_swag = models.BooleanField("Can get swag", default=False)

    can_get_swag2 = models.BooleanField("Can get swag 2", default=False)
    got_swag2 =  models.BooleanField("Got swag 1", default=False)

    createTimestamp = models.DateTimeField(
        _("Date the attendee registered"),
        auto_now_add=True
    )
    modifyTimestamp = models.DateTimeField(
        _("Date the attendee updated their record"),
        auto_now=True
    )

    class Meta:
        app_label = 'debconf_website'
        ordering = ('user__username',)
        permissions = (
            ("manage_registration", "Can manage registrations"),
        )

    def __unicode__(self):
        try:
            if self.badge_full:
                return "%s (%s)" % (self.user.username, self.badge_full)
            return "%s" % self.user.username
        except:
            return "Unknown Profile"


def user_is_dd(self):
    if self.email and self.email.endswith('@debian.org'):
        return True
    else:
        return False

