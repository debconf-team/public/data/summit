$(document).ready(function() {
  $("#wireframe .container").mouseenter(function() {
    var wf = $(this);
    wf.css({border:"1px dashed #555", padding:"0px 9px"});
    wf.parent().is("header") && wf.append('<div class="label" style="position:absolute; top:0px; left:0px; background-color:rgba(0,0,0,0.7); padding:10px; color:#fff;"><h3>HEADER</h3></div>');
    wf.parent().is("aside") && wf.append('<div class="label" style="position:absolute; top:0px; left:0px; background-color:rgba(0,0,0,0.7); padding:10px; color:#fff;"><h3>ASIDE</h3></div>');
    wf.parent().is("section") && wf.append('<div class="label" style="position:absolute; top:0px; left:0px; background-color:rgba(0,0,0,0.7); padding:10px; color:#fff;"><h3>CONTENT</h3></div>');
    wf.parent().is("footer") && wf.append('<div class="label" style="position:absolute; top:0px; left:0px; background-color:rgba(0,0,0,0.7); padding:10px; color:#fff;"><h3>FOOTER</h3></div>')
  });
  $("#wireframe .container").mouseleave(function() {
    $(".label").remove();
    $(this).css({border:"0px dashed #555", padding:"0px 10px"})
  })
});

