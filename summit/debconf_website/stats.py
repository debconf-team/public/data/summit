from django.shortcuts import render_to_response, get_object_or_404
from debconf_website.models import (
	UserProfile,
	Sponsorship,
	DebianRole,
	DebConfRole,
	RegistrationLevel,
        FoodAccommodation,
        TShirtSize,
        Diet,
        Sex,
	user_is_dd
)
from summit.schedule.models import Summit
import datetime

from summit.schedule.forms import Registration
from summit.sponsor.forms import SponsorshipScoreForm

from django.db import models
from django.forms import HiddenInput, ValidationError, FileField, ModelForm, BaseForm, NullBooleanSelect
from django.forms.models import modelformset_factory
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.template import RequestContext, Context, loader
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from summit.schedule.decorators import summit_required, summit_only_required

@login_required
@summit_only_required
def by_days(request, summit):
    deltaday = datetime.timedelta(days=1)
    notes = """\n\nNOTES\n
About hours: timeframe between arrival and departures need to be within the following frame (an instantaneous overlap is enough)
  - attend,reconfirm,prof,corp: 9h-19h
  - lunch: 12h-14h
  - dinner: 18h-20h
  - accom: 3h(next day)-4h(next day)  (it is also a proxy for breakfast)

"""
    # the replace() takes local time.

    csv = "REGISTRATION DATA:, reconfirm=Yes\n\n"

    fields = "%14s,%7s,%7s,%9s,%4s,%6s,%6s,%5s,%9s\n"

    csv += fields % ("DoW,Date      ",  "Attend", "Confirm",  "Arrivals", "Deps",  "Lunch", "Dinner", "Accom",  "WaitList")
    csv += "\n"

    br = 0
    for day in summit.staff_days():
        yesterday = (day - datetime.timedelta(days=1)).replace(hour=2, minute=0, second=0, microsecond=0)
        tomorrow = day + datetime.timedelta(days=1)
        br += 1
        if br > 5:
            csv += "\n"
            br = 0
        csv += fields % (
            datetime.datetime.strftime(day, "%a,%Y-%m-%d"),

            UserProfile.objects.filter(summit=summit).filter(attend=True).filter(start_utc__lte=day.replace(hour=19)).filter(end_utc__gte=day.replace(hour=9)).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(start_utc__lte=day.replace(hour=19)).filter(end_utc__gte=day.replace(hour=9)).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(start_utc__gte=day.replace(hour=3)).filter(start_utc__lte=tomorrow.replace(hour=3)).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(end_utc__gte=day.replace(hour=3)).filter(end_utc__lte=tomorrow.replace(hour=3)).count(),
               

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=14)).filter(end_utc__gte=day.replace(hour=12)).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=20)).filter(end_utc__gte=day.replace(hour=18)).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,4]).filter(start_utc__lte=tomorrow.replace(hour=4)).filter(end_utc__gte=tomorrow.replace(hour=3)).count(),

            UserProfile.objects.filter(summit=summit).filter(attend=True).filter(food_accommodation=5).filter(start_utc__lte=tomorrow.replace(hour=4)).filter(end_utc__gte=tomorrow.replace(hour=3)).count()
        )

    csv += "\n\nFOOD DETAILED\n\n"
    fields = "%14s,%6s,%6s,%6s,%6s,%9s,%6s,%6s,%6s,%9s\n"
    csv += fields % ("DoW,Date      ",  "L-norm", "L-vegi", "L-Vegan", "L-Spec", "D-norm", "D-vegi", "D-Vegan", "D-Spec", "Breakfast")
    csv += "\n"

    br = 0
    for day in summit.staff_days():
        yesterday = (day - datetime.timedelta(days=1)).replace(hour=2, minute=0, second=0, microsecond=0)
        tomorrow = day + datetime.timedelta(days=1)
        br += 1
        if br > 5:
            csv += "\n"
            br = 0
        csv += fields % (
            datetime.datetime.strftime(day, "%a,%Y-%m-%d"),


            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=14)).filter(end_utc__gte=day.replace(hour=12)).filter(diet=1).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=14)).filter(end_utc__gte=day.replace(hour=12)).filter(diet=2).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=14)).filter(end_utc__gte=day.replace(hour=12)).filter(diet=3).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=14)).filter(end_utc__gte=day.replace(hour=12)).filter(diet=4).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=20)).filter(end_utc__gte=day.replace(hour=18)).filter(diet=1).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=20)).filter(end_utc__gte=day.replace(hour=18)).filter(diet=2).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=20)).filter(end_utc__gte=day.replace(hour=18)).filter(diet=3).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day.replace(hour=20)).filter(end_utc__gte=day.replace(hour=18)).filter(diet=4).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,4]).filter(start_utc__lte=tomorrow.replace(hour=4)).filter(end_utc__gte=tomorrow.replace(hour=3)).count(),
        )



    csv += "\n\nBUDGET\n\nDay,Date, Prof,Corp, split1, req_sp_accom,req_sp_camp,req_sp_travel, sp_accom, got_accom,got_camp,got_travel\n\n"

    br = 0
    for day in summit.staff_days():
        yesterday = (day - datetime.timedelta(days=1)).replace(hour=2, minute=0, second=0, microsecond=0)
        tomorrow = day + datetime.timedelta(days=1)
        br += 1
        if br > 5:
            csv += "\n"
            br = 0
        csv += "%s, %2s,%2s, \"sponsor:\", %3s,%2s,%2s, %3s, %3s,%2s,%2s\n" % (
            datetime.datetime.strftime(day, "%a,%Y-%m-%d"),

            UserProfile.objects.filter(summit=summit).filter(attend=True).filter(registration_level=2).filter(start_utc__lte=day.replace(hour=19)).filter(end_utc__gte=day.replace(hour=9)).count(),
            UserProfile.objects.filter(summit=summit).filter(attend=True).filter(registration_level=3).filter(start_utc__lte=day.replace(hour=19)).filter(end_utc__gte=day.replace(hour=9)).count(),

            Sponsorship.objects.filter(summit=summit).filter(userprofile__attend=True).filter(needs_accomodation=True).filter(userprofile__start_utc__lte=tomorrow.replace(hour=4)).filter(userprofile__end_utc__gte=tomorrow.replace(hour=3)).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__attend=True).filter(needs_debcamp=True).filter(userprofile__start_utc__lte=tomorrow.replace(hour=4)).filter(userprofile__end_utc__gte=tomorrow.replace(hour=3)).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__attend=True).filter(needs_travel=True).filter(userprofile__start_utc__lte=tomorrow.replace(hour=4)).filter(userprofile__end_utc__gte=tomorrow.replace(hour=4)).count(),

            UserProfile.objects.filter(summit=summit).filter(attend=True).filter(food_accommodation=4).filter(start_utc__lte=tomorrow.replace(hour=4)).filter(end_utc__gte=tomorrow.replace(hour=3)).count(),

            Sponsorship.objects.filter(summit=summit).filter(userprofile__attend=True).filter(got_accommodation="Y").filter(userprofile__start_utc__lte=tomorrow.replace(hour=4)).filter(userprofile__end_utc__gte=tomorrow.replace(hour=3)).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__attend=True).filter(got_debcamp="Y").filter(userprofile__start_utc__lte=tomorrow.replace(hour=4)).filter(userprofile__end_utc__gte=tomorrow.replace(hour=3)).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__attend=True).filter(got_travel="Y").filter(userprofile__start_utc__lte=tomorrow.replace(hour=4)).filter(userprofile__end_utc__gte=tomorrow.replace(hour=3)).count(),

        )

    csv += "\n\nRECONFIRM\n\nDay,Date, Attend,Confirm,Arrived, food,accom, Prof,Corp, req_sp_accom,req_sp_camp,req_sp_travel, sp_accom, got_accom,got_camp,got_travel,  waiting_list\n\n"

    br = 0
    for day in summit.staff_days():
        br += 1
        if br > 5:
            csv += "\n"
            br = 0
        csv += "%s, %3s,%3s,%3s, %3s,%3s, %2s,%2s, %3s,%2s,%2s, %3s, %3s,%2s,%2s, %2s\n" % (
            datetime.datetime.strftime(day, "%a,%Y-%m-%d"),

            UserProfile.objects.filter(summit=summit).filter(attend=True).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),
            UserProfile.objects.filter(summit=summit).filter(arrived=True).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,3,4]).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation__in=[2,4]).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(registration_level=2).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),
            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(registration_level=3).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),

            Sponsorship.objects.filter(summit=summit).filter(userprofile__reconfirm=True).filter(needs_accomodation=True).filter(userprofile__start_utc__lte=day).filter(userprofile__end_utc__gte=day).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__reconfirm=True).filter(needs_debcamp=True).filter(userprofile__start_utc__lte=day).filter(userprofile__end_utc__gte=day).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__reconfirm=True).filter(needs_travel=True).filter(userprofile__start_utc__lte=day).filter(userprofile__end_utc__gte=day).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation=4).filter(start_utc__lte=day).filter(end_utc__gte=day).count(),

            Sponsorship.objects.filter(summit=summit).filter(userprofile__reconfirm=True).filter(got_accommodation="Y").filter(userprofile__start_utc__lte=day).filter(userprofile__end_utc__gte=day).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__reconfirm=True).filter(got_debcamp="Y").filter(userprofile__start_utc__lte=day).filter(userprofile__end_utc__gte=day).count(),
            Sponsorship.objects.filter(summit=summit).filter(userprofile__reconfirm=True).filter(got_travel="Y").filter(userprofile__start_utc__lte=day).filter(userprofile__end_utc__gte=day).count(),

            UserProfile.objects.filter(summit=summit).filter(reconfirm=True).filter(food_accommodation=5).filter(start_utc__lte=day).filter(end_utc__gte=day).count()
        )

    return HttpResponse(csv+notes, content_type='text/plain; charset=utf-8')

