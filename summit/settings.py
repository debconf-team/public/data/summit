# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Django settings for The Summit Scheduler project.

import os

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

DEBUG = False
TEMPLATE_DEBUG = DEBUG
SERVE_STATIC = True

SITE_ROOT = 'http://summit.ubuntu.com'

"""
In order for Summit to authenticate to Launchpad, a cookie must be created
The cookie data must then be entered into local_settings.py as LP_AUTH_COOKIE
This will be reqired to start with lp=
See below for details where XXXXXX would be replaced with the cookie data
LP_AUTH_COOKIE = "lp=XXXXXX"
"""

ADMINS = (
    #('Your Name', 'admin@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'summit',
        'USER': 'summit',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    },
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'UTC'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = False

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# Location for collectstatic to place static media
STATIC_ROOT = "static/"

# URL Location for static media
STATIC_URL = "/static/"

SECRET_KEY = ''

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "common.context_processors.next_summit",
    "common.context_processors.login_redirect",
    "common.context_processors.login_url",
    "common.context_processors.url_base",
    "common.context_processors.summit_version",
    "common.context_processors.site_menu",
    "common.context_processors.track_display_name",
    "social.apps.django_app.context_processors.backends",
    "social.apps.django_app.context_processors.login_redirect",
)

MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'summit.urls'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_PATH, 'templates'),
    os.path.join(PROJECT_PATH, 'common/templates'),
)

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django_openid_auth',
    'social.apps.django_app.default',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'south',
    'reversion',
]

LOCAL_APPS = [
    'schedule',
    'sponsor',
    'common',
]

INSTALLED_APPS += LOCAL_APPS

TEST_RUNNER = "local_tests.LocalAppsTestSuiteRunner"

AUTHENTICATION_BACKENDS = (
    'social-backends.debian.DebianOAuth2',
    'django_openid_auth.auth.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# OPENID Related settings
OPENID_STRICT_USERNAMES = True
OPENID_FOLLOW_RENAMES = True
OPENID_SREG_REQUIRED_FIELDS = ['email']
OPENID_CREATE_USERS = True
OPENID_REUSE_USERS = True
OPENID_UPDATE_DETAILS_FROM_SREG = True
OPENID_SSO_SERVER_URL = 'https://login.ubuntu.com/'

# Tell django.contrib.auth to use the OpenID signin URLs.
LOGIN_URL = '/openid/login'
LOGIN_REDIRECT_URL = '/'

LAUNCHPAD_CACHE = os.path.join(PROJECT_PATH, 'lp-cache')

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# Days before the start of the summit when we stop allowing
# track leads to edit the schedule
TRACK_LEAD_SCHEDULE_BLACKOUT = 7

# Manage apps from bzr branches
try:
    import bzr_apps
    INSTALLED_APPS.append('bzr_apps')
except:
    pass

BZR_APPS = {
    ## ubuntu-django-foundations app management
    'bzr_apps': ('http://bazaar.launchpad.net/~django-foundations-dev/ubuntu-django-foundations/bzr_apps', '7'),

    ## ubuntu-website supplied templates and styles
    'ubuntu_website': ('http://bazaar.launchpad.net/~ubuntu-community-webthemes/ubuntu-community-webthemes/light-django-theme', '70'),

    ## linaro-website supplied templates and styles
    'linaro_website': ('http://bazaar.launchpad.net/~linaro-connect-theme-devs/ubuntu-community-webthemes/light-django-linaro-theme', '52'),
}

import logging
try:
    from local_settings import *
except ImportError:
    logging.warning(
        "No local_settings.py were found. See INSTALL for instructions."
    )

WEBCHAT_URL = 'https://kiwiirc.com/client/irc.freenode.com/%(channel_name)s'
