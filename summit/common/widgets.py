# -*- coding: utf-8 -*-
from datetime import time
from time import strptime, strftime

from django import forms


class DateWidget(forms.DateInput):
    """
    A more-friendly date widget with a pop-up calendar.
    """
    def __init__(self, attrs=None):
        self.date_class = 'datepicker'
        if not attrs:
            attrs = {}
        if 'date_class' in attrs:
            self.date_class = attrs.pop('date_class')
        if 'class' not in attrs:
            attrs['class'] = 'date'

        super(DateWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None):
        return '<span class="%s">%s</span>' % (
            self.date_class,
            super(DateWidget, self).render(name, value, attrs)
        )


class TimeWidget(forms.MultiWidget):
    """
    A more-friendly time widget.
    """
    def __init__(self, attrs=None):
        self.time_class = 'timepicker'
        if not attrs:
            attrs = {}
        if 'time_class' in attrs:
            self.time_class = attrs.pop('time_class')
        if 'class' not in attrs:
            attrs['class'] = 'time'

        widgets = (
            forms.Select(
                attrs=attrs,
                choices=[(i, "%02d" % i) for i in range(0, 24)],
            ),
            forms.Select(
                attrs=attrs,
                choices=[(i, "%02d" % i) for i in range(00, 60, 15)],
            ),
            # to handle AM, PM, see older commits (DC14)
        )

        super(TimeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if isinstance(value, str):
            try:
                value = strptime(value, '%H:%M:%S')
            except ValueError:
                value = strptime(value, '%H:%M')
            hour = int(value.tm_hour)
            minute = int(value.tm_min)
            return (hour, minute)

        elif isinstance(value, time):
            hour = int(value.strftime("%H"))
            minute = int(value.strftime("%M"))
            return (hour, minute)
        return (None, None)

    def value_from_datadict(self, data, files, name):
        value = super(TimeWidget, self).value_from_datadict(data, files, name)
        if value[0] == None:
            return None
        t = strptime(
            "%02d:%02d" % (
                int(value[0]),
                int(value[1])
            ),
            "%H:%M",
        )
        return strftime("%H:%M:%S", t)

    def format_output(self, rendered_widgets):
        return '<span class="%s">%s%s</span>' % (
            self.time_class,
            rendered_widgets[0], rendered_widgets[1]
        )


class DateTimeWidget(forms.SplitDateTimeWidget):
    """
    A more-friendly date/time widget.

    Inspired by:

    http://copiesofcopies.org/webl/2010/04/26/a-better-datetime-widget-for-django/
    """
    def __init__(self, attrs=None, date_format=None, time_format=None):
        super(DateTimeWidget, self).__init__(attrs, date_format, time_format)
        self.widgets = (
            DateWidget(attrs=attrs),
            TimeWidget(attrs=attrs),
        )

    def decompress(self, value):
        if value:
            d = strftime("%Y-%m-%d", value.timetuple())
            t = strftime("%H:%M", value.timetuple())
            return (d, t)
        else:
            return (None, None)

    def format_output(self, rendered_widgets):
        return '%s%s' % (rendered_widgets[0], rendered_widgets[1])
