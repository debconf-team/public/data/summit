from django.core.management import call_command
from django.core.management.base import BaseCommand

from settings import LOCAL_APPS


class Command(BaseCommand):
    """ test_apps command. """

    def handle(self, *args, **options):
        call_command('test', *LOCAL_APPS)
