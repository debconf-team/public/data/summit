# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError

from summit.schedule.models import Summit
from optparse import make_option

__all__ = (
    'Command',
)


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            "-s",
            "--slots",
            dest="slots",
            help="Number of slots an imported meeting needs",
            type=int,
            default=1
        ),
        make_option(
            "-A",
            "--no-attendees",
            dest="skip_attendees",
            action="store_true",
            help="Don't import Attendee data",
            default=False
        ),
        make_option(
            "-M",
            "--no-meetings",
            dest="skip_meetings",
            action="store_true",
            help="Don't import Meeting data",
            default=False
        ),
    )

    def handle(self, summit='', *args, **options):
        if not summit:
            for summit in Summit.objects.all():
                print "Updating %s" % summit
                summit.update_from_launchpad(options)
        else:
            try:
                summit = Summit.objects.get(name=summit)
            except Summit.DoesNotExist:
                raise CommandError("Summit doesn't exist: %s" % summit)
            print "Updating %s" % summit
            summit.update_from_launchpad(options)
