# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import re
import urllib2
import datetime

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

from summit.schedule.models import Summit, Attendee
from summit.common.launchpad import set_user_openid
from django_openid_auth.models import UserOpenID
from django.contrib.auth.models import User

__all__ = (
    'Command',
)


class Command(BaseCommand):
    help = "Add Linaro attendees from an output from the Linaro registration form"
    option_list = BaseCommand.option_list + (
        make_option("-s", "--summit",
            dest="summit",
            help="Supply a summit."),
        make_option("-f", "--file", dest="file",
            help="Attendees File"),
        make_option("-u", "--url", dest="url",
            help="Attendees File"),
    )

    def handle(self, *args, **options):
        summit = options["summit"]
        self.verbose = int(options["verbosity"])

        try:
            summit = Summit.objects.get(name=summit)
        except Summit.DoesNotExist:
            raise CommandError("Summit doesn't exist: %s" % summit)
        if self.verbose >= 2:
            print "Importing attendees into %s" % summit

        if 'file' in options and options["file"] is not None:
            attendee_file = options["file"]
            attendees = csv.reader(open(attendee_file, 'r'), delimiter=',', quotechar='"')
        elif 'url' in options and options["url"] is not None:
            req = urllib2.Request(options["url"])
            req.add_header("Cache-Control", "no-cache")
            req.add_header("Cookie", "please-don't-cache-me")
            try:
                attendees = csv.reader(urllib2.urlopen(req), delimiter=',', quotechar='"')
            except Exception, e:
                raise CommandError("Error while reading remote attendees file: %s" % e)
        else:
            raise CommandError("You must supply a file or url to import attendees from")
            

        identity_url = re.compile(r'^https://login.launchpad.net/\+id/(?P<id>.*)$')
        for attendee in attendees:
            if attendee[2] == 'launchpad_username':
                # Skip column header
                continue

            sso = identity_url.match(attendee[1])
            username = attendee[2]

            if username is None or username == "":
                if self.verbose >= 2:
                    print "Looking up user by SSO id: %s" % sso.group('id')
                user, created = self.get_user_by_sso_id(sso.group('id'))
            else:
                if self.verbose >= 2:
                    print "Looking up user by LP username: %s" % username
                user, created = self.get_user_by_username(username.strip())

            display_name = unicode(attendee[0], 'ascii', 'replace').split(' ', 1)
            user.first_name = display_name[0]
            if len(display_name) > 1:
                user.last_name = display_name[1]
            user.save()

            if created and self.verbose >= 1:
                print "Created User: %s" % user.username

            if attendee[3] == '0000-00-00':
                attendee[3] = summit.date_start.strftime('%Y-%m-%d')
            local_start = summit.as_localtime(datetime.datetime.strptime(attendee[3]+' '+attendee[4], '%Y-%m-%d %H:%M:%S'))
            if attendee[5] == '0000-00-00':
                attendee[5] = summit.date_start.strftime('%Y-%m-%d')
            local_end = summit.as_localtime(datetime.datetime.strptime(attendee[5]+' '+attendee[6], '%Y-%m-%d %H:%M:%S'))
            a, created = Attendee.objects.get_or_create(
                summit=summit,
                user=user,
                defaults={
                'start_utc': summit.delocalize(local_start),
                'end_utc': summit.delocalize(local_end),
                })
            if created and self.verbose >= 1:
                print "Added Attendee: %s" % user.username

    def get_user_by_username(self, username):
        user, created = User.objects.get_or_create(username=username)
        set_user_openid(user)
        return (user, created)

    def get_user_by_sso_id(self, sso_id):
        sso_url = 'https://login.launchpad.net/+id/%s' % sso_id
        try:
            openid = UserOpenID.objects.get(claimed_id=sso_url)
            return (openid.user, False)
        except UserOpenID.DoesNotExist:
            username = 'launchpad-%s' % sso_id
            user, created = User.objects.get_or_create(username=username)
            UserOpenID.objects.create(user=user, claimed_id=sso_url, display_id=sso_url)
            return (user, created)
        
