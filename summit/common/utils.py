# -*- coding: utf-8 -*-


def hex_to_rgba(value):
    """
    return the rgba version of a hex color
    """
    value = value.lstrip('#')
    lv = len(value)
    return tuple(int(value[i:i + lv / 3], 16) for i in range(0, lv, lv / 3)) + (0.5,)


def redirect(to, *args, **kwargs):
    from distutils.version import LooseVersion as V
    import django
    if V(django.get_version()) > V('1.1'):
        from django.shortcuts import redirect as red
    else:
        from shortcuts import redirect as red

    return red(to, *args, **kwargs)
