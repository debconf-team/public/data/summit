# The Summit Scheduler web application
# Copyright (C) 2008 - 2013 Ubuntu Community, Canonical Ltd
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import datetime
from django import test as djangotest
from model_mommy import mommy as factory
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site

from summit.schedule.fields import NameField
from summit.schedule.models import Summit
from summit.common.models import Menu

# Monkey-patch our NameField into the types of fields that the factory
# understands.  This is simpler than trying to subclass the Mommy
# class directly.
factory.default_mapping[NameField] = str


class MenuTestCase(djangotest.TestCase):

    def setUp(self):
        site = factory.make_one(Site, id=1)
        summit = factory.make_one(
            Summit,
            name='uds-test',
            date_start=datetime.datetime.now(),
            date_end=datetime.datetime.now(),
        )
        summit.sites.add(site)
        menu = factory.make_one(Menu, name='Main', slug="main", site=site)
        menu.menuitem_set.create(
            order=0,
            link_url='/uds-test/',
            title='Schedule',
        )
        menu.menuitem_set.create(
            order=0,
            link_url='/today/uds-test/',
            title='Today',
        )

    def test_toplevel_menu_highlights(self):
        schedule_page = self.client.get('/uds-test/')
        self.assertContains(
            schedule_page,
            '<li class=\'active\' id="main-nav"><a class="main-nav-item" ' +
            'href="/uds-test/" title="Schedule">Schedule</a>',
            1,
        )
        self.assertContains(
            schedule_page,
            '<li id="main-nav"><a class="main-nav-item" href="/uds-test/" ' +
            'title="Schedule">Schedule</a>',
            0,
        )
        self.assertContains(
            schedule_page,
            '<li class=\'active\' id="main-nav"><a class="main-nav-item" ' +
            'href="/today/uds-test/" title="Today">Today</a>',
            0,
        )
        self.assertContains(
            schedule_page,
            '<li id="main-nav"><a class="main-nav-item" ' +
            'href="/today/uds-test/" title="Today">Today</a>',
            1,
        )

    def test_sublevel_menu_highlights(self):
        today_page = self.client.get('/today/uds-test/')
        self.assertContains(
            today_page,
            '<li class=\'active\' id="main-nav"><a class="main-nav-item" ' +
            'href="/uds-test/" title="Schedule">Schedule</a>',
            0,
        )
        self.assertContains(
            today_page,
            '<li id="main-nav"><a class="main-nav-item" href="/uds-test/" ' +
            'title="Schedule">Schedule</a>',
            1,
        )
        self.assertContains(
            today_page,
            '<li class=\'active\' id="main-nav"><a class="main-nav-item" ' +
            'href="/today/uds-test/" title="Today">Today</a>',
            1,
        )
        self.assertContains(
            today_page,
            '<li id="main-nav"><a class="main-nav-item" ' +
            'href="/today/uds-test/" title="Today">Today</a>',
            0,
        )


class IndexRedirectTestCase(djangotest.TestCase):

    def test_index_redirects_correct_summit(self):
        """
        Test that when loading / it redirects to the newest summit event
        """
        site = factory.make_one(Site, id=1)
        summit1 = factory.make_one(
            Summit,
            name='uds-test1',
            date_start='2011-05-10',
            date_end='2011-05-15',
        )
        summit1.sites.add(site)
        summit2 = factory.make_one(
            Summit,
            name='uds-test2',
            date_start='2013-05-10',
            date_end='2013-05-15',
        )
        summit2.sites.add(site)
        summit3 = factory.make_one(
            Summit,
            name='uds-test3',
            date_start='2012-05-10',
            date_end='2012-05-15',
        )
        summit3.sites.add(site)
        index = self.client.get(
            reverse(
                'summit.common.views.index',
            ),
        )
        response = reverse(
            'summit.schedule.views.summit',
            args=(summit2.name,)
        )
        self.assertRedirects(
            index,
            response,
        )
        response = self.client.get(response)
        self.assertContains(response, summit2.title)

    def test_no_summit_no_redirect(self):
        """
        Tests that if a summit does not exist, the index page displays a
        no summit page, not redirects
        """
        index = self.client.get(
            reverse(
                'summit.common.views.index',
            ),
        )
        self.assertEqual(index.status_code, 200)
        self.assertTemplateUsed(index, 'common/no_summit.html')
