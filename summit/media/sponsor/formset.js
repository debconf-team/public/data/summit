function addEvent(obj, evType, fn, useCapture) {
    if (obj.addEventListener) {
	obj.addEventListener(evType, fn, useCapture);
	return true;
    } else if (obj.attachEvent) {
	var r = obj.attachEvent("on" + evType, fn);
	return r;
    } else {
	alert("Handler could not be attached");
    }
}

function removeEvent(obj, evType, fn, useCapture) {
    if (obj.removeEventListener) {
	obj.removeEventListener(evType, fn, useCapture);
	return true;
    } else if (obj.detachEvent) {
	var r = obj.detachEvent("on" + evType, fn);
	return r;
    } else {
	alert("Handler could not be removed");
    }
}


var Formset = {
    total: null,
    row: null,
    fields: [],

    init: function() {
	// Locate the total field and _last_ URL field
	var url_field;
	var inputs = document.getElementsByTagName('INPUT');
	for (var i = 0; i < inputs.length; i++) {
	    var input = inputs[i];

	    if (input.getAttribute('type') == 'text'
		&& input.className.match(/BrainstormURLField/)) {
		url_field = input;
	    }

	    if (input.getAttribute('type') == 'hidden'
		&& input.name == '1-TOTAL_FORMS') {
		Formset.total = input;
	    }
	}

	// Step up to find the row
	var row = url_field;
	while (row && row.tagName != 'TR') {
	    row = row.parentNode;
	}

	Formset.row = row;
	Formset.gatherFields(row, null);
    },

    gatherFields: function(row, func) {
	// Remove event listeners
	for (var i = 0; i < Formset.fields.length; i++) {
	    var field = Formset.fields[i];

	    removeEvent(field, 'focus', Formset.appendNew, false);
	}

	Formset.fields = [];

	// Gather input fields
	var inputs = row.getElementsByTagName('INPUT');
	for (var i = 0; i < inputs.length; i++) {
	    var input = inputs[i];

	    if (func)
		func(input);
	    Formset.fields.push(input);
	}

	// Gather select fields
	var selects = row.getElementsByTagName('SELECT');
	for (var i = 0; i < selects.length; i++) {
	    var select = selects[i];

	    if (func)
		func(select);
	    Formset.fields.push(select);
	}

	// Add event listeners
	for (var i = 0; i < Formset.fields.length; i++) {
	    var field = Formset.fields[i];

	    addEvent(field, 'focus', Formset.appendNew, false);
	    addEvent(field, 'change', Formset.fixTotal, false);
	}
    },

    newField: function(field) {
	var name_re = /^([0-9]+-)([0-9]+)(-)/;
	var id_re = /^(id_[0-9]+-)([0-9]+)(-)/;

	// Increment the number in the name and id
	var found = field.name.match(name_re);
	var new_name = found[1] + (Number(found[2]) + 1) + found[3];
	field.name = field.name.replace(name_re, new_name);

	var found = field.id.match(id_re);
	var new_id = found[1] + (Number(found[2]) + 1) + found[3];
	field.id = field.id.replace(id_re, new_id);

	// Reset the value too
	if (field.tagName == 'INPUT') {
	    field.value = '';
	} else if (field.tagName == 'SELECT') {
	    field.selectedIndex = 0;
	}

    },

    appendNew: function(ev) {
	// Copy the row
	var new_row = Formset.row.cloneNode(true);
	Formset.row.parentNode.appendChild(new_row);
	Formset.gatherFields(new_row, Formset.newField);
	Formset.row = new_row;

	// Get rid of any errors
	var uls = Formset.row.getElementsByTagName('UL');
	for (var i = 0; i < uls.length; i++) {
	    var ul = uls[i];

	    if (ul.className.match(/errorlist/))
		ul.parentNode.removeChild(ul);
	}

    	// Get rid of any results
	var ps = Formset.row.getElementsByTagName('P');
	for (var i = 0; i < ps.length; i++) {
	    var p = ps[i];

	    if (p.className.match(/result/))
		p.parentNode.removeChild(p);
	}
    },

    fixTotal: function(ev) {
	var name_re = /^([0-9]+-)([0-9]+)(-)/;
	var found = this.name.match(name_re);

	var field_num = Number(found[2]);
	var total = Number(Formset.total.value);

	if (field_num >= total) {
	    Formset.total.value = field_num + 1;
	}
    }
};

addEvent(window, 'load', Formset.init, false);
