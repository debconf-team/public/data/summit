function Ajax(method, url, callback, data, params)
{
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
	if (request.readyState != 4)
	    return;

	if (request.status != 200) {
	    alert("Oh bugger: " + request.status + " - " + request.statusText);
	    return;
	}

	if (callback)
	    callback(request.responseXML.documentElement, data);
    };

    request.open(method, url, true);
    request.send(params);
}
