import os
import sys

sys.path.insert(0,'/srv/django-1.3')
root = os.path.join(os.path.dirname(__file__), '..')
sys.path.append(root)
sys.path.append(os.path.join(root, 'summit'))

os.environ['DJANGO_SETTINGS_MODULE'] = 'linaro_settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
