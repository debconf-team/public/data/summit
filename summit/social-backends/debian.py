"""
Debian OAuth2 SSO backend
Based on the Google backend for python-social-auth
"""
from requests import HTTPError

from social.backends.oauth import BaseOAuth2
from social.exceptions import AuthMissingParameter, AuthCanceled


class DebianOAuth2(BaseOAuth2):
    """Debian OAuth2 authentication backend"""
    name = 'debian-oauth2'
    REDIRECT_STATE = False
    AUTHORIZATION_URL = 'https://sso.debian.org/o/authorize'
    ACCESS_TOKEN_URL = 'https://sso.debian.org/o/token/'
    ACCESS_TOKEN_METHOD = 'POST'
    # FIXME: unconfirmed
    REVOKE_TOKEN_URL = 'https://sso.debian.org/o/revoke'
    REVOKE_TOKEN_METHOD = 'GET'
    DEFAULT_SCOPE = ['openid email profile']
    EXTRA_DATA = [
        ('refresh_token', 'refresh_token', True),
        ('expires_in', 'expires'),
        ('token_type', 'token_type', True)
    ]

    def revoke_token_params(self, token, uid):
        return {'token': token}

    def revoke_token_headers(self, token, uid):
        return {'Content-type': 'application/json'}

    def get_user_id(self, details, response):
        """Use Debian email as unique id"""
        if self.setting('USE_UNIQUE_USER_ID', False):
            return response['id']
        else:
            return details['email']

    def get_user_details(self, response):
        """Return user details from Debian account"""
        email = response.get('email', '')
        return {'username': email.split('@', 1)[0],
                'email': email,
                'fullname': response.get('name', ''),
                'first_name': response.get('given_name', ''),
                'last_name': response.get('family_name', '')}

    def user_data(self, access_token, *args, **kwargs):
        """Return user data from Debian SSO API"""
        return self.get_json(
            'https://sso.debian.org/api/v1/people/getOpenIdConnect',
            params={'access_token': access_token, 'alt': 'json'}
        )
