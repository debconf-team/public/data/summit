====================
Using the Summit API
====================

The Summit Scheduler provides a read-only JSON/REST API for all of it's schedule data.

URL Structure
=============
All of the Summit API calls follow a similar URL structure for accessing a collection or resources or an individual one:

http://summit.ubuntu.com/api/<resource_type>/<resource_id>

If you omit the <resource_id>, you will get a javascript array of a resources for the given <resource_type>

Filtering your results
======================
When requesting an array of resources, you can pass a number of key=value options in the query string to filter which resourced are returned:

http://summit.ubuntu.com/api/<resource_type>/?<field_name>=<value>

If the field is a reference to another resource type in Summit, you can extend your filter to a field on that reference by placing two underscores (_) between the primary resource type's field name and the referenced resource type's field name:

http://summit.ubuntu.com/api/<resource_type>/?<resource_type_field_name>__<referenced_resource_field_name>=<value>

Available Resources
===================
The Summit Scheduler provdes the following resources via the API:

 * Summit_
 * Track_
 * User_
 * Attendee_
 * Crew_
 * Participant_
 * Room_
 * Meeting_
 * Slot_
 * Agenda_

Summit
------
http://summit.ubuntu.com/api/summit/

The Summit object represents an individual event in the Summit Scheduler system, for example: UDS-Q.

 * **name** - Slug-like name of the summit, used to build URL paths, for example: uds-q
 * **title** - Displayed name for the summit
 * **location** - Displayed location of the summit, for example: Orlando, Florida
 * **description** - Displayed description of the summmit
 * **etherpad** - URL to the etherpad host used by the summit, for example: http://pad.ubuntu.com/
 * **qr** - URL to the QR code image
 * **hashtag** - Optional hashtag to use when promoting this summit on social networks
 * **timezone** - Local timezone where the summit is taking place, used to convert between the stored UTC time and displayed local time
 * **last_update** - Last time the summit record was updated
 * **state** - Current status of the event
 * **date_start** - Date (in localtime) when the event begins
 * **date_end** - Date (in localtime) when the event ends
 * **managers** - List of ids for User_ objects who have access to manage this summit
 * **schedulers** - List of ids for User_ objects who have access to change this summit's schedule
 * **help_text** - Text to display at the bottom of each page telling attendees who to contact for help

Track
-----
http://summit.ubuntu.com/api/track/

The Track object represents a theme or category to which Meetings belong.  Each Summit may have one or more Tracks.

 * **summit** - ID of the Summit_ object this track belongs to
 * **title** - Display name of this track
 * **slug** - Slug-like name of this track, used to build URL paths
 * **color** - Hexadecimal color code (without the leading #) that is used to highlight meetings in this track
 * **description** - Description of this track
 * **allow_adjacent_sessions** - Boolean specifying whether to schedule back-to-back meetings on this track in the same room

User
----
http://summit.ubuntu.com/api/user/

The User object represents a unique user of the Summit Scheduler, independent of any given event

 * **username** - Username for this user
 
Attendee
--------
http://summit.ubuntu.com/api/attendee/

The Attendee object represents an individual User_ who is attending an individual Summit_ event.

 * **summit** - ID of the Summit_ object this attendee is going to
 * **user** - ID of the User_ object that is attending
 * **start_utc** - Date and time (in UTC) that this attendee will first be available at the event
 * **end_utc** - Date and time (in UTC) that this attendee will last be available at the event
 * **tracks** - List of Track_ ids for tracks this attendee is interested in
 * **crew** - Boolean indicating whether this attendee has volunteered to be a crew member for this event

Crew
----
http://summit.ubuntu.com/api/crew/

The Crew object represents an Attendee_ who will be working as a member of the crew during the event

 * **attendee** - ID of the Attendee_ who will be working as a member of the crew
 * **date_utc** - Date (in UTC) when the attendee will be working as a member of the crew

Participant
-----------
http://summit.ubuntu.com/api/participant/

The Participant object represents an Attendee_ who will be a part of a given Meeting_

 * **meeting** - ID of the Meeting_ object the attendee will particpate in
 * **attendee** - ID of the Attendee_ object who will be participating in this meeting
 * **required** - Boolean indicating whether or not this attendee is required for this meeting
 * **from_launchpad** - Boolean indicating whether this attendee's participation was registered in Launchpad

Room
----
http://summit.ubuntu.com/api/room/

The Room object represents a room at the venue where the Summit_ event takes place

 * **summit** - ID of the Summit_ object this room is for
 * **name** - Slug-like name of this room, used to build URL paths
 * **title** - Display name of this room
 * **type** - Type of room, either 'open' or 'plenary'
 * **size** - Approximate number of people the room will hold
 * **tracks** - Optional list of Track_ ids that this room will be used for
 * **start_utc** - Date and time (in UTC) when this room will first be available for use
 * **end_utc** - Date and time (in UTC) when this room will last be available for use
 * **icecast_url** - URL for listening to a live audio feed from this room
 * **irc_channel** - IRC channel where remote participants can join the discussion in this room
 * **has_dial_in** - Boolean indicating whether or not this room has a phone that can be dialed into by a remote participant

Meeting
-------
http://summit.ubuntu.com/api/meeting/

The Meeting object represents a schedulable session for discussion or presentations

 * **summit** - ID of the Summit_ object this meeting is for
 * **name** - Slug-like name of this meeting, used to build URL paths
 * **title** - Display name for this meeting
 * **description** - Description stating what the meeting topic is about
 * **type** - Type of meeting this is, for example: 'plenary' or 'discussion'
 * **status** - Status of this meeting's Blueprint definition, for example: 'NEW', 'DRAFT', or 'DISCUSSION'
 * **priority** - Integer between 0 and 100 representing the relative priority of this meeting's Blueprint
 * **tracks** - List of Track_ ids that this meeting is a part of
 * **spec_url** - URL to this meeting's Blueprint or other specification = models.URLField(blank=True,
 * **wiki_url** - URL to this meeting's Wiki page
 * **pad_url** - URL to this meeting's Etherpad page
 * **slots** - Integer showing the number of Slot_ times this meeting requires
 * **override_break** - Boolean indicating whether or not this meeting can be scheduled over a break betweem slots
 * **approved** - Approval status of this meeting, for example: 'PENDING', 'APPROVED', 'DECLINED'
 * **requires_dial_in** - Boolean indicating whether or not this meeting requires a Room_ with dial-in capability
 * **video** - Boolean indicating whether or not video of this meeting will be recorded
 * **drafter** - ID of the Attendee_ who is drafting this meeting
 * **assignee** - ID of the Attendee_ who is assigned to this meeting
 * **approver** ID of the Attendee_ who must approve this meeting
 * **scribe** ID of the Attendee_ who will take notes during this meeting
 * **participants** - List of Participant_ ids for those who plan on participating in this meeting

Slot
----
http://summit.ubuntu.com/api/slot/

The Slot object represents a unit of schedulable time during the event

 * **summit** - ID of the Summit_ this time slot is for
 * **type** -  Type of time slot this is, for example: 'open', 'plenary, or 'lunch'
 * **start_utc** - Date and time (in UTC) that this slot begins
 * **end_utc** - Date and time (in UTC) that this slot ends

Agenda
------
http://summit.ubuntu.com/api/agenda/

The Agenda object represents a Meeting_ scheduled in a given time Slot_ and a given Room_

 * **slot** - ID of the time Slot_ that the meeting has been scheduled for
 * **room** - ID of the Room_ that the meeting has been scheduled in
 * **meeting** - ID of the Meeting_ that has been scheduled
 * **auto** - Boolean indicating whether or not this was created by the auto-scheduler
