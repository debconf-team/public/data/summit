====================
Scheduling a Meeting
====================

You have two options to schedule a meeting. 

You can either create a meeting in Summit or by using a blueprint in Launchpad.

----------------------------
Creating a Meeting in Summit
----------------------------

Creating a meeting in Summit is now easier than ever. All you need to do is fill out a simple form and the meeting will be created and scheduled for you automaticaly.

A video_ has been created to walk you through this process.

.. _video: http://www.youtube.com/watch?v=Jr25brEMOuA

--------------------------------
Creating a Meeting in Launchpad
--------------------------------

If you've already defined a blueprint that needs discussing you can register it. You can also create one from scratch.

Using an Existing Engineering Blueprint
-------------------------------------------

If your blueprint can fit into a single session, this is the route to choose. After creating your blueprint you may select the "Propose for sprint" option when viewing the blueprint in Launchpad.


Creating a "Scheduling Blueprint"
------------------------------------

If your engineering blueprint is too big for a single session, you can break it up by using multiple "scheduling blueprints".

To create a scheduling blueprint, you need to visit the sprint page in Launchpad. You will find a link to this page on the main page for the Summit you are attending.

Prior to creating a blueprint, first view the blueprints proposed to see if one matches the topic you want to discuss.

If none of the existing blueprints match your topic, you may register a new Blueprint by selecting the "Register a blueprint." 

When creating a blueprint, please use the naming convention below. Keep in mind that the name of a blueprint is very important as it affects how your meeting will appear in the scheduling tool at UDS. 

The format is either:

* ubuntu-<cycle>-<track>-<topic>
* linaro-<track>-<cycle>-<topic>

Further information on this topic can be found at:

* https://lists.ubuntu.com/archives/ubuntu-devel/2011-March/032813.html
* http://ubuntudevelopers.blip.tv/file/3539348/

The video is old so ignore the beginning about the wiki and just pay attention to the part where Jorge talks about Launchpad.

Filling Out the Blueprint Fields
-----------------------------------

The following are fields on the "Register a blueprint" page.

**For**

When registering a blueprint for UDS, the project will be "ubuntu."

**Name**

The name is important as it determines how your meeting will appear in the scheduling tool at UDS, here are the track names:

consumer community design desktop foundations hardware security serverandcloud ubuntu-arm

You need to name your blueprint after the track name, and the title, and then the animal letter so that we can track it for this cycle:

* hardware-p-kernel-decision 
* serverandcloud-p-openstack-integration 
* design-p-wallpapers 
* foundations-p-python-transition

... and so on.

**Title**

This should be a human readable form of the name. Remember that your title will be displayed on the monitors, and need to be short and upfront, so make it easy to understand and put the information up front so that when the scheduler truncates your long title it makes sense.

**Specification**

Optional, but it can be a link to a page in this Wiki, but it's becoming popular to put your meeting notes from etherpad here, so:

http://pad.ubuntu.com/serverandcloud-p-openstack-integration

Don't worry, the system will autocreate your etherpad during UDS.

**Definition**

The definition is required to be either  "**new**",  "**discussion**", or  "**drafting**".


------------------
What Happens Next
------------------

After you submit the blueprint, it will wait for approval by a track lead. Once it has been approved it will show up on the summit schedule:

* http://summit.ubuntu.com/
* http://summit.linaro.org/

If your blueprint is not on the schedule during the week before the summit, please ask your track lead to approve it. If they don't have it then it means you didn't submit it to the correct sprint (see the video). Track leads will be approving specs on a regular basis, so if you don't see your meetings on the schedule it's up to YOU to bother the track lead.

Ubuntu Developer Summit
-----------------------

Jono Bacon and Michael Hall keep an eye on the incoming specs for UDS, so if a track lead is particularly swamped we help keep the submissions going smoothly, but it helps tremendously when you check.

Linaro Connect
---------------

Joey Stanford fills the role of monitoring incoming specs for Linaro Connect.

------------------
Tips
------------------

**Meeting Title**

The title of the blueprint is important. We have screens setup at our events displaying the meetings for the day at various locations. For example:

http://summit.linaro.org/uds-o/2011-05-11/

A long title will be cut off on the display, however in your browser, you can hover and get the full title. This is not possible on the hallway screens and you will only be able to see the truncated text on the screens. Therefore, it becomes important to make sure the most important words show up near the beginning of your title.

**Whiteboard**

The etherpad document will most likely get used to collect notes for a meeting. This creates a small dilemma when you need to put work items in and make it a real "engineering blueprint". The recommended approach to dealing with this is to copy the contents of the notes from etherpad and paste them into the whiteboard.

**Before Creating a Blueprint**

Its a good idea to talk to the track lead before drafting a proposal and see if they like the idea.
