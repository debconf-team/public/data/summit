.. The Summit Scheduler documentation master file, created by
   sphinx-quickstart on Sun Jun 19 08:13:13 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to The Summit Scheduler documentation!
==============================================

The Summit Scheduler is a highly agile scheduling solution for conferences. Built originally for use at Ubuntu Developer Summits, the Summit Scheduler is now also used by Linaro at Linaro Connect and at the Linux Plumbers Conference.

If you are interested in using the Summit Scheduler at your conference, please contact Chris Johnston <chrisjohnston@ubuntu.com>.

Contents:

.. toctree::
   
   running
   using
   scheduling
   lead
   scheduler
   api
   credits

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

